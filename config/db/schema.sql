SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS `cultivos` (
  `id` tinyint(3) unsigned NOT NULL,
  `nombre` char(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ensayos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cultivo_id` tinyint(3) unsigned NOT NULL,
  `localidad_id` int(10) unsigned NOT NULL,
  `fecha_siembra` date DEFAULT NULL,
  `fungicida` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fecha_siembra` (`fecha_siembra`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ensayos_genotipos` (
  `ensayo_id` int(10) unsigned NOT NULL,
  `genotipo_id` int(10) unsigned NOT NULL,
  `rendimiento` int(10) unsigned NOT NULL,
  `porcentaje_proteina` float(5,2) unsigned DEFAULT NULL,
  `porcentaje_aceite` float(5,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`ensayo_id`,`genotipo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `genotipos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `localidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zona_id` tinyint(3) unsigned NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `zona_id` (`zona_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `password` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `role` varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'user',
  `mail` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `country` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `company` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `jobtitle` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL,
  `last_login` varchar(45) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE IF NOT EXISTS `zonas` (
  `id` tinyint(3) unsigned NOT NULL,
  `nombre` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
