<?php

$app->setRoute("homepage", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "index"
    )
));
$app->setRoute("como-funciona", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "comoFunciona"
    )
));
$app->setRoute("que-es", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "queEs"
    )
));
$app->setRoute("contacto", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "contacto"
    )
));
$app->setRoute("quienes-lo-hacemos", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "quienesLoHacemos"
    )
));
$app->setRoute("que-datos-incluye", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "queDatosIncluye"
    )
));
$app->setRoute("app", array(
    "controller" => "Page",
    "actions" => array(
        "default" => "app"
    )
));
?>
