<?php

namespace flowcode\qsgen\service;

use flowcode\pachamama\domain\MailMessage;
use flowcode\pachamama\service\MailService;

/**
 * @author Juan Manuel Aguero.
 */
class ContactService {

    /**
     * Envia el mail de consulta.
     * @param type $to
     * @param type $from
     * @param type $body
     * @param type $subject
     * @return type
     */
    public function enviarConsulta($to, $from, $body, $subject, $fromName) {
        $mailView = __DIR__ . "/../view/frontend/mail/contacto.view.php";
        $params["body"] = $body;
        $params["from_name"] = $fromName;
        $mail = MailMessage::getHtmlMail($mailView, $params, $to, $from, $subject);
        return MailService::send($mail);
    }

}

?>