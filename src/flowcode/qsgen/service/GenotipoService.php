<?php

namespace flowcode\qsgen\service;

use flowcode\qsgen\dao\GenotipoDao;

/**
 * @author Juan Manuel Aguero.
 */
class GenotipoService {

    private $genotipoDao = null;

    /**
     * Get genotipos by params.
     * @param type $idCultivo
     * @param type $idsZonas
     * @param type $idsCampanas
     * @param type $dateFrom
     * @param type $dateTo
     * @param type $idsGenotipos
     * @param type $fungicida
     * @return type
     */
    public function findByParams($idCultivo, $idsZonas, $idsCampanas, $dateFrom, $dateTo, $idsGenotipos = array(), $fungicida = false) {
        return $this->getGenotipoDao()->findByParams($idCultivo, $idsZonas, $idsCampanas, $dateFrom, $dateTo, $idsGenotipos, $fungicida);
    }

    /**
     * Get a genotipoDao instance.
     * @return GenotipoDao $genotipoDao.
     */
    public function getGenotipoDao() {
        if (is_null($this->genotipoDao)) {
            $this->genotipoDao = new GenotipoDao();
        }
        return $this->genotipoDao;
    }

    public function setGenotipoDao($genotipoDao) {
        $this->genotipoDao = $genotipoDao;
    }

}

?>