<?php

namespace flowcode\qsgen\service;

use flowcode\qsgen\dao\EnsayoDao;

/**
 * @author Juan Manuel Aguero.
 */
class EnsayoService {

    private $ensayoDao;

    public function getCampanas($cultivo_id, $zonas_id) {
        return $this->getEnsayoDao()->getCampanas($cultivo_id, $zonas_id);
    }

    public function getEnsayosById($id_ensayos, $offset = null, $ensayos_per_page = null) {
        return $this->getEnsayoDao()->getEnsayosById($id_ensayos, $offset, $ensayos_per_page);
    }

    public function getEnsayosGenotipos($ensayos) {
        return $this->getEnsayoDao()->getEnsayosGenotipos($ensayos);
    }

    public function getEnsayosCompartidos($cultivo_id, $zonas_id, $campanas_id, $date_from, $date_to, $genotipos_id = array(), $fungicida = FALSE) {
        return $this->getEnsayoDao()->getEnsayosCompartidos($cultivo_id, $zonas_id, $campanas_id, $date_from, $date_to, $genotipos_id, $fungicida);
    }

    public function getEnsayoDao() {
        if (is_null($this->ensayoDao)) {
            $this->ensayoDao = new EnsayoDao();
        }
        return $this->ensayoDao;
    }

    public function setEnsayoDao($ensayoDao) {
        $this->ensayoDao = $ensayoDao;
    }

}

?>