<?php

namespace flowcode\qsgen\service;

use flowcode\qsgen\dao\ZonaDao;


/**
 * @author Juan Manuel Aguero.
 */
class ZonaService {

    private $zonaDao = null;

    public function findAll() {
        return $this->getZonaDao()->findAll();
    }

    public function findByCultivoId($id) {
        return $this->getZonaDao()->findByCultivoId($id);
    }

    /**
     * Get a zonaDao instance.
     * @return ZonaDao $zonaDao.
     */
    public function getZonaDao() {
        if (is_null($this->zonaDao)) {
            $this->zonaDao = new ZonaDao();
        }
        return $this->zonaDao;
    }

    public function setZonaDao($zonaDao) {
        $this->zonaDao = $zonaDao;
    }

}

?>