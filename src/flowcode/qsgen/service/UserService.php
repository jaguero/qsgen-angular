<?php

namespace flowcode\qsgen\service;

use flowcode\enlace\Enlace;
use flowcode\pachamama\domain\MailMessage;
use flowcode\pachamama\service\MailService;
use flowcode\qsgen\dao\UserDao;
use flowcode\qsgen\domain\User;
use flowcode\wing\mvc\Config;

/**
 * @author Juan Manuel Aguero.
 */
class UserService {

    private $userDao;

    function __construct() {
        $this->userDao = new UserDao();
    }

    public function register(User $user) {
        $success = false;
        /* save user */
        if ($this->save($user)) {
            /* send confirmation mail */
            $mailView = __DIR__ . "/../view/frontend/mail/confirm.view.php";
            $params["mail"] = $user->getMail();
            $params["nombre"] = $user->getName();
            $from = Enlace::get("mail", "notifications");
            $subject = "Bienvenido a QSGen";
            $mail = MailMessage::getHtmlMail($mailView, $params, $user->getMail(), $from, $subject);
            return MailService::send($mail);
            $success = true;
        }
        return $success;
    }

    /**
     * Funcion que guarda un Usuario.
     * 
     * @return type id.
     */
    public function save(User $user) {
        $user->setPassword($this->encodePass($user->getPassword()));
        $id = $this->userDao->save($user);
        return $id;
    }

    public function encodePass($pass) {
        $encoded = sha1($pass);
        return $encoded;
    }

    public function modificarUsuario($user, $alterPass = FALSE) {
        if ($alterPass) {
            $user->setPassword($this->encodePass($user->getPassword()));
        }
        $id = $this->userDao->save($user);
        return $id;
    }

    /**
     * Realiza la validacion y el login del usuario. 
     * Returna un valor de acuerdo a si fue correcto o no.
     * @param type $mail
     * @param type $password
     * @return boolean 
     */
    public function loginUsuario($mail, $password) {
        $response = FALSE;

        if (strlen($mail) > 0 && strlen($password) > 0) {
            $user = $this->getUserByMailPassword($mail, $password);
            if ($user != null) {
                if ($user->getConfirmed()) {
                    $this->authenticateUser($user);
                    $response = User::$USER_LOGGED;
                } else {
                    $response = User::$USER_NOT_CONFIRMED;
                }
            } else {
                $response = User::$USER_NOT_REGISTERED;
            }
        }
        return $response;
    }

    /**
     * Obtiene un usuario.
     * @return User. 
     */
    public function getUserByUsernamePassword($username, $password) {
        $encodedPass = $this->encodePass($password);
        $user = $this->userDao->getUserByUsernamePassword($username, $encodedPass);
        return $user;
    }

    /**
     * 
     * @param type $mail
     * @param type $password
     * @return User suer.
     */
    public function getUserByMailPassword($mail, $password) {
        $encodedPass = $this->encodePass($password);
        $user = $this->userDao->getUserByMailPassword($mail, $encodedPass);
        return $user;
    }

    /**
     * Obtiene un usuario.
     * @return type 
     */
    public function obtenerUsuarioPorUsername($username) {
        $user = $this->userDao->obtenerUsuarioPorUsername($username);
        return $user;
    }

    /**
     * Find by user mail.
     * @param string $mail
     * @return User user.
     */
    public function findUserByMail($mail) {
        $user = $this->userDao->finUserByMail($mail);
        return $user;
    }

    /**
     * Obtiene un usuario de acuero al id.
     * @return type 
     */
    public function obtenerUsuarioPorId($id) {
        $user = $this->userDao->obtenerUsuarioPorId($id);
        return $user;
    }

    /**
     * Obtiene todos los usuarios del sistema.
     * @return type 
     */
    public function obtenerUsuariosTodos() {
        return $this->userDao->obternerUsuariosTodos();
    }

    public function obtenerUsuariosFiltrados($pagina = 1, $filtro = null) {
        $pager = null;
        $pager = null;
        $cantSlotsPorPagina = Config::get('listados', 'usuarios_por_pagina');

        $data = $this->userDao->obtenerUsuariosFiltro($pagina - 1, $filtro);

        $total = $this->userDao->obtenerTotalUsuariosFiltro($filtro);
        $cantidadPaginas = ceil($total / $cantSlotsPorPagina);

        $pager['data'] = $data;
        $pager['total'] = $total;
        $pager['page-count'] = $cantidadPaginas;
        $pager['prev'] = ($pagina > 1) ? $pagina - 1 : $pagina;
        $pager['next'] = ($pagina < $cantidadPaginas) ? $pagina + 1 : $pagina;

        return $pager;
    }

    /**
     * Elimina un usuario por su id.
     * @param type $id 
     */
    public function eliminarUsuarioPorId($id) {
        $this->userDao->eliminarUsuarioPorId($id);
    }

    /**
     * Authentica un usuario en la sesion.
     * @param type $user 
     */
    public function authenticateUser(User $user) {
        $_SESSION['user']['username'] = $user->getMail();
        $_SESSION['user']['mail'] = $user->getMail();

        foreach ($user->getRoles() as $role) {
            foreach ($role->getPermissions() as $permission) {
                $_SESSION['user']['roles'][$role->getName()]["permissions"][] = $permission->getName();
            }
        }
    }

    public function resetPassword($mail) {

        $reset = User::$USER_NOT_REGISTERED;

        $user = $this->findUserByMail($mail);

        if (!is_null($user)) {
            if ($user->getConfirmed()) {
                if ($user->getMail() == $mail) {

                    /* update password */
                    $randomPass = substr(md5(rand()), 0, 7);
                    $user->setPassword($randomPass);
                    $id = $this->modificarUsuario($user, TRUE);
                    /* send confirmation mail */
                    $mailView = __DIR__ . "/../view/frontend/mail/reset-password.view.php";
                    $params["mail"] = $user->getMail();
                    $params["randomPass"] = $randomPass;
                    $params["from_name"] = "QSgen Notificaciones";
                    $from = Enlace::get("mail", "notifications");
                    $subject = "Reset Password";
                    $mail = MailMessage::getHtmlMail($mailView, $params, $user->getMail(), $from, $subject);
                    if (MailService::send($mail)) {
                        $reset = User::$USER_RESET;
                    }
                }
            } else {
                $reset = User::$USER_NOT_CONFIRMED;
            }
        }

        return $reset;
    }

    public function findByFilter($filter = null, $page = 1) {
        $pager = $this->userDao->findByFilter($filter, $page);
        return $pager;
    }

    public function getUserDao() {
        return $this->userDao;
    }

    public function setUserDao($userDao) {
        $this->userDao = $userDao;
    }

    public function findRoles(User $user) {
        return $this->userDao->findRoles($user);
    }

    public function findById($id) {
        return $this->userDao->findById($id);
    }

}

?>
