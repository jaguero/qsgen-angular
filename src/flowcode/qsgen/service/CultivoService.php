<?php

namespace flowcode\qsgen\service;

use flowcode\qsgen\dao\CultivoDao;


/**
 * @author Juan Manuel Aguero.
 */
class CultivoService {

    private $cultivoDao = null;

    public function findAll() {
        return $this->getCultivoDao()->findAll();
    }

    /**
     * Get a cultivoDao instance.
     * @return CultivoDao $cultivoDao.
     */
    public function getCultivoDao() {
        if (is_null($this->cultivoDao)) {
            $this->cultivoDao = new CultivoDao();
        }
        return $this->cultivoDao;
    }

    public function setCultivoDao($cultivoDao) {
        $this->cultivoDao = $cultivoDao;
    }

}

?>