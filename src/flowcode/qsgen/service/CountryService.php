<?php

namespace flowcode\qsgen\service;

use flowcode\qsgen\dao\CountryDao;
use flowcode\qsgen\domain\Country;

/**
 * @author Juan Manuel Aguero.
 */
class CountryService {

    private $countryDao = null;

    /**
     * Get all countrys.
     * @return array countrys.
     */
    public function findAll() {
        return $this->getCountryDao()->findAll();
    }

    /**
     * 
     * @param type $code
     * @return Country country.
     */
    public function findByCode($code) {
        return $this->getCountryDao()->findByCode($code);
    }

    /**
     * Get a CountryDao instance.
     * @return CountryDao dao.
     */
    public function getCountryDao() {
        if (is_null($this->countryDao)) {
            $this->countryDao = new CountryDao();
        }
        return $this->countryDao;
    }

    public function setCountryDao($countryDao) {
        $this->countryDao = $countryDao;
    }

}

?>