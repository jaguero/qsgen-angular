<?php

namespace flowcode\qsgen\form;

use flowcode\roble\form\BaseForm;
use flowcode\roble\form\field\InputField;
use flowcode\roble\form\validator\StringFieldValidator;

/**
 * Description of RegisterForm
 *
 * @author juanma
 */
class EditPasswordForm extends BaseForm {

    function __construct() {
        parent::__construct();
        $this->addField("password-old", new InputField(null, array("class" => "form-control", "type" => "password")), new StringFieldValidator(250, 2));
        $this->addField("password-new", new InputField(null, array("class" => "form-control", "type" => "password")), new StringFieldValidator(250, 2));
        $this->addField("password-confirm", new InputField(null, array("class" => "form-control", "type" => "password")), new StringFieldValidator(250, 2));
    }

}
