<?php

namespace flowcode\qsgen\form;

use flowcode\qsgen\domain\User;
use flowcode\qsgen\service\CountryService;
use flowcode\roble\form\BaseForm;
use flowcode\roble\form\field\InputField;
use flowcode\roble\form\field\SelectField;
use flowcode\roble\form\validator\MailValidator;
use flowcode\roble\form\validator\StringFieldValidator;

/**
 * Description of RegisterForm
 *
 * @author juanma
 */
class EditProfileForm extends BaseForm {

    function __construct(User $user = null) {
        parent::__construct();
        $this->addField("nombre", new InputField(null, array("class" => "form-control", "placeholder" => "Nombre")), new StringFieldValidator(250, 2));
        $this->addField("mail", new InputField(null, array("class" => "form-control", "placeholder" => "Email")), new MailValidator());
        $this->addField("pais", new SelectField(array("empty_first" => true), array("class" => "form-control", "placeholder" => "pais")), new StringFieldValidator(250, 2));
        $this->addField("compania", new InputField(null, array("class" => "form-control", "placeholder" => "compania")), new StringFieldValidator(250, 2));
        $this->addField("cargo", new InputField(null, array("class" => "form-control", "placeholder" => "cargo")), new StringFieldValidator(250, 2));
        if (!is_null($user)) {
            $this->getField("nombre")->setValue($user->getName());
            $this->getField("mail")->setValue($user->getMail());
            $this->getField("compania")->setValue($user->getCompany());
            $this->getField("cargo")->setValue($user->getJobTitle());
            $this->getField("pais")->setValue($user->getCountry());
        }
        $countrySrv = new CountryService();
        $countryList = $countrySrv->findAll();
        $this->getField("pais")->addOption("data", $countryList);
    }

}
