<?php

namespace flowcode\qsgen\form;

use flowcode\roble\form\BaseForm;
use flowcode\roble\form\field\InputField;
use flowcode\roble\form\field\TextField;
use flowcode\roble\form\validator\MailValidator;
use flowcode\roble\form\validator\StringFieldValidator;

/**
 * Description of SampleForm
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
class ContactForm extends BaseForm {

    function __construct() {
        parent::__construct();
        $this->addField("name", new InputField(null, array("class" => "form-control", "placeholder" => "Nombre")), new StringFieldValidator(250, 2));
        $this->addField("mail", new InputField(null, array("class" => "form-control", "placeholder" => "Mail")), new MailValidator());
        $this->addField("coment", new TextField(null, array("class" => "form-control", "placeholder" => "Consulta")), new StringFieldValidator(2000, 4));
    }

}

?>
