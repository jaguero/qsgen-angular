<?php

namespace flowcode\qsgen\form;

use flowcode\roble\form\BaseForm;
use flowcode\roble\form\field\InputField;
use flowcode\roble\form\validator\MailValidator;
use flowcode\roble\form\validator\StringFieldValidator;

/**
 * Description of RegisterForm
 *
 * @author juanma
 */
class LoginForm extends BaseForm {

    function __construct() {
        parent::__construct();
        $this->addField("mail", new InputField(null, array("class" => "form-control", "placeholder" => "Email")), new MailValidator());
        $this->addField("password", new InputField(null, array("class" => "form-control", "placeholder" => "Password", "type" => "password")), new StringFieldValidator(250, 2));
    }

}
