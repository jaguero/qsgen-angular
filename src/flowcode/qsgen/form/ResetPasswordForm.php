<?php

namespace flowcode\qsgen\form;

use flowcode\roble\form\BaseForm;
use flowcode\roble\form\field\InputField;
use flowcode\roble\form\validator\MailValidator;

/**
 * Description of RegisterForm
 *
 * @author juanma
 */
class ResetPasswordForm extends BaseForm {

    function __construct() {
        parent::__construct();
        $this->addField("mail", new InputField(null, array("class" => "form-control", "placeholder" => "Email", "id" => "mail")), new MailValidator());
    }

}
