<?php

namespace flowcode\qsgen\dao;

use flowcode\ceibo\EntityManager;
use flowcode\enlace\Enlace;
use flowcode\qsgen\domain\Permission;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Description of GenotipoDao
 *
 * @author Juanma.
 */
class GenotipoDao {

    public function __construct() {
        
    }

    public function findByParams($idCultivo, $idsZonas, $idsCampanas, $dateFrom, $dateTo, $idsGenotipos = array(), $fungicida = false) {
        $sql_zonas_id = implode(", ", $idsZonas);
        $sql_campanas_id = implode(", ", $idsCampanas);
        $sql1 = ' ';
        $sql2 = ' ';
        $sql_fungicida = ' ';

        if (!empty($idsGenotipos)) {
            $sql_genotipos_id = implode(", ", $idsGenotipos);

            $sql1 = 'AND ge.genotipo_id NOT IN (' . $sql_genotipos_id . ') ';
            $sql2 = 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $idsGenotipos[0] . ') ';

            if (!empty($idsGenotipos[1])) {
                $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $idsGenotipos[1] . ') ';
            }
            if (!empty($idsGenotipos[2])) {
                $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $idsGenotipos[2] . ') ';
            }
            if (!empty($idsGenotipos[3])) {
                $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $idsGenotipos[3] . ') ';
            }
            if (!empty($idsGenotipos[4])) {
                $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $idsGenotipos[4] . ') ';
            }
        }
        if ($fungicida === '0' || $fungicida === '1') {
            $sql_fungicida = 'AND e.fungicida = ' . $fungicida . ' ';
        }
        $statement = 'SELECT g.id, g.nombre FROM genotipos g ';
        $statement .= 'JOIN ensayos_genotipos as ge ON ge.genotipo_id = g.id ' . $sql1;
        $statement .= 'JOIN ensayos as e ON e.id = ge.ensayo_id ';
        $statement .= 'JOIN localidades as l ON l.id = e.localidad_id ';
        $statement .= 'WHERE ( DAY(e.fecha_siembra) >= DAY(:dateFrom) AND MONTH(e.fecha_siembra) >= MONTH(:dateFrom) ) ';
        $statement .= 'AND (DAY(e.fecha_siembra) <= DAY(:dateTo) AND MONTH(e.fecha_siembra) <= MONTH(:dateTo) )';
        $statement .= 'AND e.cultivo_id = :idCultivo AND YEAR(e.fecha_siembra) IN (' . $sql_campanas_id . ')';
        $statement .= 'AND l.zona_id IN (' . $sql_zonas_id . ') ';
        $statement .= $sql_fungicida . $sql2;
        $statement .= 'GROUP BY g.id ORDER BY g.nombre ASC';
        
        $log = new Logger('Jmmm');
        $file = Enlace::get("dir", "log") . "/log-otro-" . date("Ymd") . ".txt";
        $log->pushHandler(new StreamHandler($file, Logger::ERROR));
        $log->addError($statement);
        
        return EntityManager::getInstance()->getDataSource()->query($statement, array(":idCultivo" => $idCultivo, ":dateFrom" => $dateFrom, ":dateTo" => $dateTo));
    }

    public function save(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->save($permission);
    }

    function delete(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->delete($permission);
    }

    public function findById($id) {
        $em = EntityManager::getInstance();
        return $em->findById("cultivo", $id);
    }

    public function findAll() {
        $em = EntityManager::getInstance();
        $statement = "SELECT cultivos.* ";
        $statement .= "FROM cultivos ";
        return $em->getDataSource()->query($statement);
    }

}

?>
