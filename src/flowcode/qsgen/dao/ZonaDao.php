<?php

namespace flowcode\qsgen\dao;

use flowcode\ceibo\EntityManager;
use flowcode\miweb\domain\Permission;

/**
 * Description of PermissionDao
 *
 * @author Juanma.
 */
class ZonaDao {

    public function __construct() {
        
    }

    public function findByCultivoId($id) {
        $em = EntityManager::getInstance();
        $statement = "SELECT zonas.id, zonas.nombre ";
        $statement .= "FROM zonas ";
        $statement .= "INNER JOIN  localidades ON localidades.zona_id = zonas.id ";
        $statement .= "INNER JOIN  ensayos ON ensayos.localidad_id = localidades.id ";
        $statement .= "WHERE ensayos.cultivo_id = $id ";
        $statement .= "GROUP BY zonas.id ";
        $statement .= "ORDER BY zonas.id ASC";

        $result = $em->getDataSource()->query($statement);
        return $result;
    }

    public function save(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->save($permission);
    }

    function delete(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->delete($permission);
    }

    public function findById($id) {
        $em = EntityManager::getInstance();
        return $em->findById("permission", $id);
    }

    public function findAll() {
        $em = EntityManager::getInstance();
        $statement = "SELECT cultivos.* ";
        $statement .= "FROM cultivos ";
        return $em->getDataSource()->query($statement);
    }

    /**
     * Return an active permissionr.
     * @param type $filter
     * @param type $permission
     * @return Permission $permission.
     */
    public function findByFilter($filter = null, $permission = 1) {
        $em = EntityManager::getInstance();
        $permissionr = $em->findByGenericFilter("permission", $filter, $permission, "name", "asc");
        return $permissionr;
    }

}

?>
