<?php

namespace flowcode\qsgen\dao;

use flowcode\ceibo\EntityManager;
use flowcode\enlace\Enlace;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Description of GenotipoDao
 *
 * @author Juanma.
 */
class EnsayoDao {

    public function __construct() {
        
    }

    public function getCampanas($cultivo_id, $zonas_id) {
        $stmt = "SELECT YEAR(e.fecha_siembra) as campana ";
        $stmt .= "FROM ensayos e ";
        $stmt .= "INNER JOIN localidades lo ON lo.id = e.localidad_id ";
        $stmt .= "WHERE e.cultivo_id = :cultivo_id ";
        $stmt .= "AND lo.zona_id IN (" . implode(", ", $zonas_id) . ") ";
        $stmt .= "GROUP BY campana ";
        $stmt .= "ORDER BY campana asc";
        return EntityManager::getInstance()->getDataSource()->query($stmt, array(":cultivo_id" => $cultivo_id));
    }

    /**
     * Busca los ensayos compartidos entre distintos genotipos.
     * @param type $cultivo_id
     * @param type $zonas_id
     * @param type $campanas_id
     * @param type $date_from
     * @param type $date_to
     * @param type $genotipos_id
     * @param type $fungicida
     * @return type
     */
    public function getEnsayosCompartidos($cultivo_id, $zonas_id, $campanas_id, $date_from, $date_to, $genotipos_id = array(), $fungicida = FALSE) {
        $sql_zonas_id = implode(", ", $zonas_id);
        $sql_campanas_id = implode(", ", $campanas_id);
        $sql1 = ' ';
        $sql2 = ' ';
        $sql_fungicida = ' ';
        foreach ($genotipos_id as $k => $v) {
            if (empty($genotipos_id[$k]))
                unset($genotipos_id[$k]);
        }
        $sql_genotipos_id = implode(", ", $genotipos_id);
        $sql1 = 'AND ge.genotipo_id IN (' . $sql_genotipos_id . ') ';
        $sql2 = 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $genotipos_id[0] . ') ';
        if (!empty($genotipos_id[1]))
            $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $genotipos_id[1] . ') ';
        if (!empty($genotipos_id[2]))
            $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $genotipos_id[2] . ') ';
        if (!empty($genotipos_id[3]))
            $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $genotipos_id[3] . ') ';
        if (!empty($genotipos_id[4]))
            $sql2 .= 'AND e.id IN (SELECT ensayo_id FROM ensayos_genotipos WHERE ensayos_genotipos.genotipo_id = ' . $genotipos_id[4] . ') ';
        if ($fungicida === '0' || $fungicida === '1') {
            $sql_fungicida = 'AND e.fungicida = ' . $fungicida . ' ';
        }
        $statement = 'SELECT g.nombre, ge.* FROM genotipos g';
        $statement .=' JOIN ensayos_genotipos as ge ON ge.genotipo_id = g.id ';
        $statement .= 'JOIN ensayos as e ON e.id = ge.ensayo_id ';
        $statement .= 'JOIN localidades as l ON l.id = e.localidad_id ';
        $statement .= 'WHERE e.cultivo_id = :cultivo_id AND YEAR(e.fecha_siembra) IN (' . $sql_campanas_id . ') ';
        $statement .= 'AND (DAY(e.fecha_siembra) >= DAY(:dateFrom) AND MONTH(e.fecha_siembra) >= MONTH(:dateFrom) ) ';
        $statement .= 'AND (DAY(e.fecha_siembra) <= DAY(:dateTo) AND MONTH(e.fecha_siembra) <= MONTH(:dateTo)) ';
        $statement .= 'AND l.zona_id IN (' . $sql_zonas_id . ') ';
        $statement .= $sql1;
        $statement .= $sql_fungicida . $sql2;
        $statement .= ' ORDER BY g.nombre ASC, ge.ensayo_id ASC';

        return EntityManager::getInstance()->getDataSource()->query($statement, array(":cultivo_id" => $cultivo_id, ":dateFrom" => $date_from, ":dateTo" => $date_to));
    }

    /**
     * 
     * @param array $id_ensayos
     * @param integer $offset
     * @param integer $ensayos_per_page
     */
    public function getEnsayosById($id_ensayos, $offset = null, $ensayos_per_page = null) {
        $ensayos = implode(", ", $id_ensayos);
        $statement = 'SELECT zonas.nombre as zona, localidades.nombre as localidad, DATE_FORMAT(e.fecha_siembra, "%Y") as campana, DATE_FORMAT(e.fecha_siembra, "%d/%m/%Y") as fecha_siembra, e.id as ensayo_id ';
        $statement .= 'FROM ensayos as e ';
        $statement .= 'INNER JOIN cultivos ON cultivos.id = e.cultivo_id ';
        $statement .= 'INNER JOIN localidades ON localidades.id = e.localidad_id ';
        $statement .= 'INNER JOIN zonas ON zonas.id = localidades.zona_id ';
        $statement .= 'WHERE e.id IN  (' . $ensayos . ') ';
        $statement .= 'ORDER BY e.fecha_siembra DESC, ensayo_id DESC ';
//        $statement .= 'LIMIT ' . $ensayos_per_page . ' ' . $offset;
        $array = EntityManager::getInstance()->getDataSource()->query($statement);
        $ensayo = $array[0];
        $a = explode('/', $ensayo["fecha_siembra"]);
        if ($a[1] == '01') {
            $ensayo["campana"] = (intval($a[2]) - 1);
        }
        return $array;
    }

    /**
     * 
     * @param array $ensayos
     * @return type
     */
    public function getEnsayosGenotipos($ensayos) {

        $ensayos_in = '';
        foreach ($ensayos as $e) {
            $ensayos_in .= $e . ',';
        }
        $ensayos_in = substr($ensayos_in, 0, strlen($ensayos_in) - 1);

        $statement = 'SELECT genotipos.nombre, eg.*, ensayos.fecha_siembra ';
        $statement .= 'FROM ensayos_genotipos as eg ';
        $statement .= 'INNER JOIN ensayos ON ensayos.id = eg.ensayo_id ';
        $statement .= 'INNER JOIN genotipos ON genotipos.id = eg.genotipo_id ';
        $statement .= 'WHERE eg.ensayo_id IN  (' . $ensayos_in . ') ';
        $statement .= 'ORDER BY ensayos.fecha_siembra desc, eg.ensayo_id desc, eg.rendimiento desc';

        $ensayos_genotipos = EntityManager::getInstance()->getDataSource()->query($statement);

        $i = 0;
        $en_id = $ensayos_genotipos[0]["ensayo_id"];
        $formateado = array();
        foreach ($ensayos_genotipos as $eg) {
            if ($eg["ensayo_id"] != $en_id) {
                $i = 0;
                $en_id = $eg["ensayo_id"];
            }
            $formateado[$eg["ensayo_id"]][$i]['nombre'] = $eg["nombre"];
            $formateado[$eg["ensayo_id"]][$i]['genotipo_id'] = $eg["genotipo_id"];
            $formateado[$eg["ensayo_id"]][$i]['rendimiento'] = $eg["rendimiento"];
            $formateado[$eg["ensayo_id"]][$i]['proteina'] = $eg["porcentaje_proteina"];
            $formateado[$eg["ensayo_id"]][$i]['aceite'] = $eg["porcentaje_aceite"];
            $i++;
        }
        return $formateado;
    }

}

?>
