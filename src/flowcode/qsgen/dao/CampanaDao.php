<?php

namespace flowcode\qsgen\dao;

use flowcode\ceibo\EntityManager;
use flowcode\miweb\domain\Permission;

/**
 * Description of PermissionDao
 *
 * @author Juanma.
 */
class CultivoDao {

    public function __construct() {
        
    }

    public function save(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->save($permission);
    }

    function delete(Permission $permission) {
        $em = EntityManager::getInstance();
        $em->delete($permission);
    }

    public function findById($id) {
        $em = EntityManager::getInstance();
        return $em->findById("cultivo", $id);
    }

    public function findAll() {
        $em = EntityManager::getInstance();
        $statement = "SELECT cultivos.* ";
        $statement .= "FROM cultivos ";
        return $em->getDataSource()->query($statement);
    }

}

?>
