<?php

namespace flowcode\qsgen\dao;

use flowcode\qsgen\domain\Country;

/**
 * Description of PermissionDao
 *
 * @author Juanma.
 */
class CountryDao {

    private $countryList = null;

    public function __construct() {
        
    }

    public function findAll() {
        return $this->getCountryList();
    }

    public function findByCode($code) {
        $country = null;
        $countrys = $this->getCountryList();
        if (isset($countrys[$code])) {
            $country = $countrys[$code];
        }
        return $country;
    }

    public function getCountryList() {
        if (is_null($this->countryList)) {
            $this->countryList = array();

            $xmlpaises = 'http://ws.geonames.org/countryInfo?lang=es';
            $paises = simplexml_load_file($xmlpaises);

            foreach ($paises->country as $pais) {
                $this->countryList[(string) $pais->countryCode] = new Country($pais->countryCode, $pais->countryName);
            }
        }
        return $this->countryList;
    }

    public function setCountryList($countryList) {
        $this->countryList = $countryList;
    }

}

?>
