<?php

namespace flowcode\qsgen\domain;

/**
 * Usuario de la aplicacion.
 *
 * @author Juan Manuel Aguero.
 */
class Resultado {

    private $genotipos = array();
    private $ensayos = array();
    private $rendimiento_promedio = 0;
    private $cantidad_genotipos = 0;
    private $cantidad_ensayos = 0;

    public function __construct() {
        
    }

    /**
     * Realiza los calculos para mostrar en la vista.
     * @param type $datasource
     */
    public function calcular($datasource) {
        $i = 0;
        $gen_id = $datasource[0]["genotipo_id"];
        foreach ($datasource as $d) {
            if ($d["genotipo_id"] != $gen_id) {
                $i++;
                $gen_id = $d["genotipo_id"];
            }
            $this->genotipos[$i]['nombre'] = $d["nombre"];
            $this->genotipos[$i]['id'] = $d["genotipo_id"];
            $this->genotipos[$i]['ensayos'][$d["ensayo_id"]]['rendimiento'] = $d["rendimiento"];
            $this->genotipos[$i]['ensayos'][$d["ensayo_id"]]['aceite'] = $d["porcentaje_aceite"];
            $this->genotipos[$i]['ensayos'][$d["ensayo_id"]]['proteina'] = $d["porcentaje_proteina"];
        }

        $this->cantidad_genotipos = count($this->genotipos);
        $this->cantidad_ensayos = count($this->genotipos[0]['ensayos']);
        $this->calcular_promedios();
        $this->calcular_rectas();
    }

    /**
     * Calcula los promedios de rendimientos de ensayos.
     */
    public function calcular_promedios() {
        foreach ($this->genotipos as $k => $g) {
            $suma_rendimiento = 0;
            $suma_aceite = 0;
            $suma_proteina = 0;
            $ensayos_aceite = 0;
            $ensayos_proteina = 0;
            foreach ($g['ensayos'] as $e) {
                $suma_rendimiento += $e['rendimiento'];
                if (!is_null($e['aceite'])) {
                    $suma_aceite += $e['aceite'];
                    $ensayos_aceite++;
                }
                if (!is_null($e['proteina'])) {
                    $suma_proteina += $e['proteina'];
                    $ensayos_proteina++;
                }
            }
            $this->genotipos[$k]['rendimiento_promedio'] = round($suma_rendimiento / $this->cantidad_ensayos);
            if ($ensayos_aceite > 0)
                $this->genotipos[$k]['aceite_promedio'] = number_format($suma_aceite / $ensayos_aceite,1);
            if ($ensayos_proteina > 0)
                $this->genotipos[$k]['proteina_promedio'] = number_format($suma_proteina / $ensayos_proteina,1);
        }

        //calcula el promedio de rendimiento por ensayo
        $g_id = key($this->genotipos);
        foreach ($this->genotipos[$g_id]['ensayos'] as $ensayo_id => $e) {
            $this->ensayos[$ensayo_id]['rendimiento_promedio'] = 0;
            foreach ($this->genotipos as $genotipo_id => $g) {
                $this->ensayos[$ensayo_id]['rendimiento_promedio'] += $g['ensayos'][$ensayo_id]['rendimiento'];
            }
            $this->ensayos[$ensayo_id]['rendimiento_promedio'] /= $this->cantidad_genotipos;
        }
    }

    /**
     * 
     */
    public function calcular_rectas() {
        $sum = 0;
        foreach ($this->ensayos as $e) {
            $sum += $e['rendimiento_promedio'];
        }
        $this->rendimiento_promedio = $sum / $this->cantidad_ensayos;

        $aux_ensayos = array();
        $suma_ensayos = 0;
        foreach ($this->ensayos as $ensayo_id => $e) {
            $aux_ensayos[$ensayo_id]['resta_promedios'] = $e['rendimiento_promedio'] - $this->rendimiento_promedio;
            $aux_ensayos[$ensayo_id]['cuadrado'] = pow($aux_ensayos[$ensayo_id]['resta_promedios'], 2);
            $suma_ensayos += $aux_ensayos[$ensayo_id]['cuadrado'];
        }

        $aux_genotipos = array();
        foreach ($this->genotipos as $k => $g) {
            $aux_genotipos[$k]['suma'] = 0;
            foreach ($g['ensayos'] as $ensayo_id => $e) {
                $aux_genotipos[$k]['ensayos'][$ensayo_id]['distancia'] = $e['rendimiento'] - $g['rendimiento_promedio'];
                $aux_genotipos[$k]['ensayos'][$ensayo_id]['multi'] = $aux_genotipos[$k]['ensayos'][$ensayo_id]['distancia'] * $aux_ensayos[$ensayo_id]['resta_promedios'];
                $aux_genotipos[$k]['suma'] += $aux_genotipos[$k]['ensayos'][$ensayo_id]['multi'];
            }
        }

        foreach ($this->genotipos as $k => $g) {
            $this->genotipos[$k]['pendiente_recta'] = ($suma_ensayos > 0) ? $aux_genotipos[$k]['suma'] / $suma_ensayos : $aux_genotipos[$k]['suma'];
            $this->genotipos[$k]['pendiente_recta'] = round($this->genotipos[$k]['pendiente_recta'], 2);
            $this->genotipos[$k]['interseccion'] = $g['rendimiento_promedio'] - $this->genotipos[$k]['pendiente_recta'] * $this->rendimiento_promedio;
            $this->genotipos[$k]['interseccion'] = round($this->genotipos[$k]['interseccion'], 1);
        }
    }

    /**
     * 
     * @return type
     */
    public function get_id_ensayos() {
        return array_keys($this->ensayos);
    }

    public function getGenotipos() {
        return $this->genotipos;
    }

    public function getEnsayos() {
        return $this->ensayos;
    }

    public function getRendimiento_promedio() {
        return $this->rendimiento_promedio;
    }

    public function getCantidad_genotipos() {
        return $this->cantidad_genotipos;
    }

    public function getCantidad_ensayos() {
        return $this->cantidad_ensayos;
    }

    public function setGenotipos($genotipos) {
        $this->genotipos = $genotipos;
    }

    public function setEnsayos($ensayos) {
        $this->ensayos = $ensayos;
    }

    public function setRendimiento_promedio($rendimiento_promedio) {
        $this->rendimiento_promedio = $rendimiento_promedio;
    }

    public function setCantidad_genotipos($cantidad_genotipos) {
        $this->cantidad_genotipos = $cantidad_genotipos;
    }

    public function setCantidad_ensayos($cantidad_ensayos) {
        $this->cantidad_ensayos = $cantidad_ensayos;
    }

}

?>
