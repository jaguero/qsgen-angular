<?php

namespace flowcode\qsgen\domain;

/**
 * Description of UserMessage
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
class UserMessage {

    private $type;
    private $message;
    private $title;
    public static $WARNING = "warning";
    public static $INFO = "info";
    public static $ERROR = "fail";
    public static $SUCCESS = "success";

    /**
     * UserMessage.
     * types: success, danger, warning, info.
     * @param type $type
     * @param string $message
     * @param string $title optional.
     */
    function __construct($type, $message, $title = null) {
        $this->type = $type;
        $this->message = $message;
        if (!is_null($title)) {
            $this->title = $title;
        }
    }

    public function getMessage() {
        return $this->message;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function __toString() {
        $str = '<div class="alert alert-' . $this->type . '">';
        if (!is_null($this->title)) {
            $str .= '<strong>' . ucfirst($this->type) . '!</strong> ';
        }
        $str .= $this->message;
        $str .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        $str .= '</div>';
        return $str;
    }

}

?>
