<?php

namespace flowcode\qsgen\domain;

use flowcode\qsgen\service\UserService;

/**
 * Usuario de la aplicacion.
 *
 * @author Juan Manuel Aguero.
 */
class User {

    private $id;
    private $name;
    private $country;
    private $company;
    private $jobTitle;
    private $username;
    private $password;
    private $confirmed;
    private $lastLoginDate;
    private $roles;
    private $mail;
    public static $USER_NOT_REGISTERED = 0;
    public static $USER_NOT_CONFIRMED = 1;
    public static $USER_LOGGED = 2;
    public static $USER_RESET = 3;

    public function __construct() {
        $this->roles = NULL;
    }

    public function __toString() {
        return $this->name;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getCompany() {
        return $this->company;
    }

    public function getJobTitle() {
        return $this->jobTitle;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

    public function setJobTitle($jobTitle) {
        $this->jobTitle = $jobTitle;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        if (!is_null($username) && is_string($username)) {
            $this->username = $username;
        }
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        if (!is_null($password) && is_string($password)) {
            $this->password = $password;
        }
    }

    public function getConfirmed() {
        return $this->confirmed;
    }

    public function getLastLoginDate() {
        return $this->lastLoginDate;
    }

    public function setConfirmed($confirmed) {
        $this->confirmed = $confirmed;
    }

    public function setLastLoginDate($lastLoginDate) {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * Get roles.
     * @return \Iterator roles.
     */
    public function getRoles() {
        if ($this->roles == NULL) {
            $userSrv = new UserService();
            $this->roles = $userSrv->findRoles($this);
        }
        return $this->roles;
    }

    public function getRolesNames() {
        $roles = "";
        $rolesCollection = $this->getRoles();
        if ($rolesCollection->count() > 0) {
            foreach ($rolesCollection as $role) {
                $roles .= $role->getName() . ", ";
            }
        }
        return $roles;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }

    public function setMail($mail) {
        $this->mail = $mail;
    }

    public function getMail() {
        return $this->mail;
    }

}

?>
