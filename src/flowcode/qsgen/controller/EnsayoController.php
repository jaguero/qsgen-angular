<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\qsgen\domain\Resultado;
use flowcode\qsgen\service\EnsayoService;

/**
 * 
 */
class EnsayoController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function campanas(HttpRequest $httpRequest) {
        $cultivo_id = $httpRequest->getParameter("cultivo");
        $zonas_id = $httpRequest->getParameter("zonas");
        $ensayoSrv = new EnsayoService();
        $campanas = $ensayoSrv->getCampanas($cultivo_id, $zonas_id);
        $viewData["data"] = $campanas;
        return new JsonView($viewData);
    }

    public function resultado(HttpRequest $httpRequest) {

        $cultivo_id = $httpRequest->getParameter("cultivo");
        $zonas_id = $httpRequest->getParameter("zonas");
        $campanas_id = $httpRequest->getParameter("campanas");
        $date_from = $httpRequest->getParameter("dateFrom");
        $date_to = $httpRequest->getParameter("dateTo");
        
        foreach ($httpRequest->getParameter("genotipos") as $genotipo) {
            $genotipos_id[] = $genotipo["id"];
        }

        $fungicida = $httpRequest->getParameter("fungicida");

        $ensayoSrv = new EnsayoService();
        $ensayosCompartidos = $ensayoSrv->getEnsayosCompartidos($cultivo_id, $zonas_id, $campanas_id, $date_from, $date_to, $genotipos_id, $fungicida);
        $resultado = new Resultado();
        $resultado->calcular($ensayosCompartidos);

        $viewData["data"] = $resultado;
        return new JsonView($viewData);
    }

    public function findAllById(HttpRequest $httpRequest) {
        $id_ensayos = $httpRequest->getParameter("ensayos");
        $ensayoSrv = new EnsayoService();
        $viewData["data"] = $ensayoSrv->getEnsayosById($id_ensayos);
        return new JsonView($viewData);
    }

    public function ensayosGenotipos(HttpRequest $httpRequest) {
        $id_ensayos = $httpRequest->getParameter("ensayos");
        $ensayoSrv = new EnsayoService();
        $viewData["data"] = $ensayoSrv->getEnsayosGenotipos($id_ensayos);
        return new JsonView($viewData);
    }

}

?>
