<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\Enlace;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\View;
use flowcode\qsgen\domain\Page;
use flowcode\qsgen\domain\UserMessage;
use flowcode\qsgen\form\ContactForm;
use flowcode\qsgen\form\RegisterForm;
use flowcode\qsgen\service\ContactService;

/**
 * 
 */
class PageController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function index(HttpRequest $httpRequest) {
        $viewData['register-form'] = new RegisterForm();
        return new View($viewData, "frontend/page/index");
    }

    public function comoFunciona(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Cómo funciona");
        return new View($viewData, "frontend/page/como-funciona");
    }

    public function queEs(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Qué es");
        return new View($viewData, "frontend/page/que-es");
    }

    public function queDatosIncluye(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Qué datos incluye");
        return new View($viewData, "frontend/page/que-datos-incluye");
    }

    public function quienesLoHacemos(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Quienes lo hacemos");
        return new View($viewData, "frontend/page/quienes-lo-hacemos");
    }
    
    public function contacto(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Contacto");
        $viewData["form"] = new ContactForm();
        return new View($viewData, "frontend/page/contacto");
    }
    
    public function contactar(HttpRequest $httpRequest) {
        $form = new ContactForm();
        $form->bind($_POST);

        if ($form->isValid()) {
            $mailsSrv = new ContactService();
            if ($mailsSrv->enviarConsulta(
                            Enlace::get("mailcontacto"), $form->getField("mail")->getValue(), $form->getField("coment")->getValue(), "Consulta ", $form->getField("name")->getValue())
            ) {
                $viewData['message'] = new UserMessage(UserMessage::$SUCCESS, "Consulta enviada.");
                $viewData["form"] = new ContactForm();
            } else {
                $viewData['message'] = new UserMessage(UserMessage::$WARNING, "Disculpe las molestias, hubo un error en el sistema.");
                $viewData["form"] = new ContactForm();
            }
        } else {
            $viewData["form"] = $form;
        }
        return new View($viewData, "frontend/page/contacto");
    }

    public function app(HttpRequest $httpRequest) {
        $viewData['page'] = new Page("Aplicacion");
        return new View($viewData, "frontend/app/index", false);
    }

    public function manage(HttpRequest $httpRequest) {
        $permalink = substr($httpRequest->getRequestedUrl(), 1);

        $page = null;
        if (strlen($permalink) <= 0) {
            $permalink = Enlace::get("homepage", "permalink");
        }

        if (is_null($page)) {
            return $this->manageNotFound($permalink);
        }

        switch ($page->getType()) {
            case Page::$type_plain:
                return $this->managePlainPage($page);
                break;
            case Page::$type_custom:
                $this->manageCustomPage($page);
                break;
            default:
                $this->manageNotFound($permalink);
                break;
        }
    }

    private function managePlainPage(Page $page) {
        $pm = new PlainPageManager($page);
        $viewData = $pm->getViewData();

        return new View($viewData, "frontend/page/plain-page");
    }

    private function manageCustomPage(Page $page) {
        die("not implemented");
        require_once "view/page/pageList.view.php";
    }

    private function manageNotFound($permalink) {
        $viewData['page'] = new Page();
        $viewData['msg'] = "";
        return new View($viewData, "frontend/page/page-not-found");
    }

    public function error() {
        $viewData['data'] = "";
        return new View($viewData, "backend/page/error-page");
    }

}

?>
