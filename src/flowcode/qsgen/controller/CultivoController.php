<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\qsgen\service\CultivoService;

/**
 * 
 */
class CultivoController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function index(HttpRequest $httpRequest) {
        $cultivoSrv = new CultivoService();
        $viewData["data"] = $cultivoSrv->findAll();
        return new JsonView($viewData);
    }

}

?>
