<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\enlace\view\View;
use flowcode\qsgen\domain\Page;
use flowcode\qsgen\domain\User;
use flowcode\qsgen\domain\UserMessage;
use flowcode\qsgen\form\EditPasswordForm;
use flowcode\qsgen\form\EditProfileForm;
use flowcode\qsgen\form\ResetPasswordForm;
use flowcode\qsgen\service\UserService;

/**
 * 
 */
class UserController extends BaseController {

    private $userSrv = null;

    function __construct() {
        $this->setIsSecure(false);
    }

    public function registration(HttpRequest $httpRequest) {
        return new JsonView($viewData);
    }

    public function profile(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Perfil");
        $user = $this->getUserSrv()->findUserByMail($_SESSION["user"]["mail"]);
        $viewData["form"] = new EditProfileForm($user);
        return new View($viewData, "frontend/user/profile");
    }

    public function updateProfile(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Perfil");
        $form = new EditProfileForm();
        $form->bind($_POST);
        if ($form->isValid()) {
            $user = $this->getUserSrv()->findUserByMail($_SESSION["user"]["mail"]);
            $user->setName($form->getField("nombre")->getValue());
            $user->setCompany($form->getField("compania")->getValue());
            $user->setCountry($form->getField("pais")->getValue());
            $user->setJobTitle($form->getField("cargo")->getValue());

            if ($this->getUserSrv()->modificarUsuario($user)) {
                $viewData["message"] = new UserMessage("success", "bien");
            } else {
                $viewData["message"] = new UserMessage("warning", "mal");
            }
            $viewData["form"] = new EditProfileForm($user);
        } else {
            $viewData["message"] = new UserMessage("warning", "mal");
            $viewData["form"] = $form;
        }

        return new View($viewData, "frontend/user/profile");
    }

    public function confirm(HttpRequest $httpRequest) {
        $mail = $httpRequest->getParameter("mail");
        $userSrv = new UserService();
        $user = $userSrv->findUserByMail($mail);

        if (is_null($user)) {
            $viewData["page"] = new Page("Usuario Inexistente");
            $viewData["page"]->setHtmlContent("El mail no existe.");
        } else {
            $user->setConfirmed(true);
            $userSrv->modificarUsuario($user);
            $viewData["page"] = new Page("Usuario confirmado");
            $viewData["page"]->setHtmlContent("Su mail ha sido verificado, ya puede iniciar sesión.");
        }
        return new View($viewData, "frontend/user/verify");
    }

    public function password(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Pasword - Perfil");
        $viewData["user"] = $this->getUserSrv()->findUserByMail($_SESSION["user"]["mail"]);
        $viewData["form"] = new EditPasswordForm();
        return new View($viewData, "frontend/user/password");
    }

    public function passChange(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Cambiar Password - Perfil");
        $form = new EditPasswordForm();
        $form->bind($_POST);
        if ($form->isValid()) {
            $user = $this->getUserSrv()->findUserByMail($_SESSION["user"]["mail"]);
            $newPass = $form->getField("password-new")->getValue();
            $encodedOldPass = $this->getUserSrv()->encodePass($form->getField("password-old")->getValue());
            if ($user->getPassword() == $encodedOldPass) {
                if ($newPass == $form->getField("password-confirm")->getValue()) {
                    $user->setPassword($newPass);
                    $passChange = $this->getUserSrv()->modificarUsuario($user, true);
                    $viewData["message"] = new UserMessage(UserMessage::$SUCCESS, "Se cambió el password con éxito.");
                } else {
                    $viewData["message"] = new UserMessage(UserMessage::$WARNING, "El nuevo password y el de confirmación no coinciden.");
                }
            } else {
                $viewData["message"] = new UserMessage(UserMessage::$WARNING, "El password anterior no concide con el password del usuario.");
            }
        }
        $viewData["form"] = $form;
        return new View($viewData, "frontend/user/password");
    }

    public function registered(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Registrado");
        return new View($viewData, "frontend/user/registered");
    }

    public function passForgot(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Recuperar Contraseña");
        $viewData["form"] = new ResetPasswordForm();
        return new View($viewData, "frontend/user/password-reset");
    }

    public function passReset(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Recuperar Contraseña");
        $form = new ResetPasswordForm();
        $form->bind($_POST);
        if ($form->isValid()) {
            $reset = $this->getUserSrv()->resetPassword($form->getField("mail")->getValue());
            switch ($reset) {
                case User::$USER_NOT_CONFIRMED:
                    $viewData["message"] = new UserMessage(UserMessage::$WARNING, "El usuario no confirmo el mail.");
                    break;
                CASE User::$USER_RESET:
                    $viewData["message"] = new UserMessage(UserMessage::$SUCCESS, "Se ha enviado un mail con datos para recuperar su contraseña.");
                    break;
                default:
                    $viewData["message"] = new UserMessage(UserMessage::$WARNING, "El mail no corresponde con un usuario registrado en el sistema.");
                    break;
            }
        }
        $viewData["form"] = $form;
        return new View($viewData, "frontend/user/password-reset");
    }

    /**
     * Return an instance.
     * @return UserService userService.
     */
    public function getUserSrv() {
        if (is_null($this->userSrv)) {
            $this->userSrv = new UserService();
        }
        return $this->userSrv;
    }

    public function setUserSrv($userSrv) {
        $this->userSrv = $userSrv;
    }

}

?>
