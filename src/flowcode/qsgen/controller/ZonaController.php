<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\qsgen\service\ZonaService;

/**
 * 
 */
class ZonaController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function index(HttpRequest $httpRequest) {
        $zonaSrv = new ZonaService();
        $viewData["data"] = $zonaSrv->findAll();
        return new JsonView($viewData);
    }

    public function cultivo(HttpRequest $httpRequest) {
        $p = $httpRequest->getParams();
        $id = $p[0];
        $zonaSrv = new ZonaService();
        $zonas = $zonaSrv->findByCultivoId($id);
        $viewData["data"] = $zonas;
        return new JsonView($viewData);
    }

}

?>
