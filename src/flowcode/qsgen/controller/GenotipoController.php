<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\qsgen\service\GenotipoService;

/**
 * 
 */
class GenotipoController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function index(HttpRequest $httpRequest) {
        $genotipoSrv = new GenotipoService();
        $viewData["data"] = $genotipoSrv->findByParams($idCultivo, $idsZonas, $idsCampanas, $dateFrom, $dateTo);
        return new JsonView($viewData);
    }

    public function find(HttpRequest $httpRequest) {
        $cultivo = $httpRequest->getParameter("cultivo");
        $zonas = $httpRequest->getParameter("zonas");
        $campanas = $httpRequest->getParameter("campanas");
        $dateFrom = $httpRequest->getParameter("dateFrom");
        $dateTo = $httpRequest->getParameter("dateTo");

        $genotipos = array();
        if (!is_null($httpRequest->getParameter("genotipos"))) {
            foreach ($httpRequest->getParameter("genotipos") as $genotipo) {
                $genotipos[] = $genotipo["id"];
            }
        }

        $fungicida = $httpRequest->getParameter("fungicida");

        $genotipoSrv = new GenotipoService();
        $viewData["data"] = $genotipoSrv->findByParams($cultivo, $zonas, $campanas, $dateFrom, $dateTo, $genotipos, $fungicida);
        return new JsonView($viewData);
    }

}

?>
