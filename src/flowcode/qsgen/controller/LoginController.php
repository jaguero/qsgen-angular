<?php

namespace flowcode\qsgen\controller;

use flowcode\enlace\controller\BaseController;
use flowcode\enlace\http\HttpRequest;
use flowcode\enlace\view\JsonView;
use flowcode\enlace\view\View;
use flowcode\qsgen\domain\Page;
use flowcode\qsgen\domain\User;
use flowcode\qsgen\domain\UserMessage;
use flowcode\qsgen\form\LoginForm;
use flowcode\qsgen\form\RegisterForm;
use flowcode\qsgen\service\UserService;

/**
 * 
 */
class LoginController extends BaseController {

    function __construct() {
        $this->setIsSecure(false);
    }

    public function index(HttpRequest $httpRequest) {
        $viewData["page"] = new Page("Login");
        $viewData["form"] = new LoginForm();
        return new View($viewData, "frontend/user/login");
    }

    public function registration(HttpRequest $httpRequest) {

        return new JsonView($viewData);
    }

    public function register(HttpRequest $httpRequest) {
        $registerForm = new RegisterForm();
        $registerForm->bind($_POST);
        if ($registerForm->isValid()) {
            $newUser = new User();
            $newUser->setMail($registerForm->getField("mail")->getValue());
            $newUser->setPassword($registerForm->getField("password")->getValue());
            $newUser->setConfirmed(false);
//            $newUser->setRoles($roles);
            $userSrv = new UserService();
            if ($userSrv->register($newUser)) {
                $this->redirect("/user/registered");
            }
        }
        $viewData['register-form'] = $registerForm;
        return new View($viewData, "frontend/page/index");
    }

    public function confirm(HttpRequest $httpRequest) {
        $mail = $httpRequest->getParameter("mail");
        $userSrv = new UserService();
        $user = $userSrv->findUserByMail($mail);
        if (is_null($user)) {
            $viewData["page"] = new Page("Usuario Inexistente");
            $viewData["page"]->setHtmlContent("El mail no existe.");
        } else {
            $user->setConfirmed(true);
            $userSrv->modificarUsuario($user);
            $viewData["page"] = new Page("Usuario confirmado");
            $viewData["page"]->setHtmlContent("Su mail ha sido verificado, ya puede iniciar sesión.");
        }
        return new View($viewData, "frontend/user/verify");
    }

    public function validlogin(HttpRequest $httpRequest) {
        $loginForm = new LoginForm();
        $loginForm->bind($_POST);
        if ($loginForm->isValid()) {
            $userSrv = new UserService();
            $response = $userSrv->loginUsuario($loginForm->getField("mail")->getValue(), $loginForm->getField("password")->getValue());
            switch ($response) {
                case User::$USER_LOGGED:
                    $this->redirect("/");
                    break;
                case User::$USER_NOT_CONFIRMED:
                    $viewData["message"] = new UserMessage(UserMessage::$INFO, "Usted no confirmó su mail, por favor hágalo  para poder loggearse.");
                    break;
                case User::$USER_NOT_REGISTERED:
                    $viewData["message"] = new UserMessage(UserMessage::$WARNING, "Combinación de mail y contraseña no válidos.");
                    break;
            }
        }
        $viewData['form'] = $loginForm;
        return new View($viewData, "frontend/user/login");
    }

    public function validMail(HttpRequest $httpRequest) {

        $mail = $httpRequest->getParameter("mail");
        if (preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $mail)) {
            $userSrv = new UserService();
            if (is_null($userSrv->findUserByMail($mail))) {
                $response = 2;
            } else {
                $response = 1;
            }
        } else {
            $response = 0;
        }
        $viewData["data"] = $response;
        return new JsonView($viewData);
    }

    public function logout(HttpRequest $httpRequest) {
        session_destroy();
        $this->redirect("/");
    }

}

?>
