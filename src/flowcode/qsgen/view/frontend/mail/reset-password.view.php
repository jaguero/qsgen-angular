<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Reset Password</title>
    </head>
    <body>
        <h3>Contraseña Restablecida</h3>
        <p>El sistema genero una nueva contraseña que encontrará mas abajo.</p>
        <p>Por favor ingrese al sistema con esta contraseña y cámbiela por una elegida por usted.</p>
        <table style="border: 1px solid #DFDFDF;background-color: #F9F9F9;width: 100%;-moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;font-family: Arial,Helvetica,Verdana,sans-serif;color: #333;width: 600px;">
            <tbody>
                <tr>
                    <td style="border-top-color: white;border-bottom: 1px solid #DFDFDF;color: #555;font-size: 12px;padding: 4px 7px 2px;vertical-align: top;">Contraseña generada</td>
                    <td style="border-top-color: white;border-bottom: 1px solid #DFDFDF;color: #555;font-size: 12px;padding: 4px 7px 2px;vertical-align: top;"><?php echo $params["randomPass"] ?></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>