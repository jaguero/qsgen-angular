<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Usuario registrado</title>
    </head>
    <body>
        <h3>Ha sido registrado, por favor verifique su mail</h3>
        <span>haga click en link de abajo...</span>
        <table style="border: 1px solid #DFDFDF;background-color: #F9F9F9;width: 100%;-moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;font-family: Arial,Helvetica,Verdana,sans-serif;color: #333;width: 600px;">
            <tbody>
                <tr>
                    <td style="border-top-color: white;border-bottom: 1px solid #DFDFDF;color: #555;font-size: 12px;padding: 4px 7px 2px;vertical-align: top;">link</td>
                    <td style="border-top-color: white;border-bottom: 1px solid #DFDFDF;color: #555;font-size: 12px;padding: 4px 7px 2px;vertical-align: top;"><a href="<?php echo "http://qsgen.com.ar/user/confirm/mail/" . $params["mail"] ?>">confirmar</a></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>