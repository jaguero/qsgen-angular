<!DOCTYPE html>
<html lang="es">
    <head>
        <?php
        $baseTitle = "QSGEN - La respuesta para la elección adecuada de genotipos";
        if (isset($viewData["page"])) {
            $title = ucfirst($viewData["page"]->getName()) . " | " . $baseTitle;
        } else {
            $title = $baseTitle;
        }
        ?>
        <title><?= $title ?></title>
        <meta name="robots" content="index, follow">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="/images/logo/qsgen-logo-cuadrado-transparente.png">

        <link rel="stylesheet" href="/css/boostrap.flatty.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" >
        <link rel="stylesheet" href="/css/frontend.css">

        <script type="text/javascript" src="//code.jquery.com/jquery.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="/js/tooltip.js"></script>

    </head>
    <body>

        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand qsgen-logo" href="/"><span>qs</span>gen</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">

                <ul class="nav navbar-nav">
                    <li><a href="/" ><i class="glyphicon glyphicon-home"></i></a></li>

                    <li><a href="/que-es" >Sobre <strong>qsgen</strong></a></li>
                    <li><a href="/quienes-lo-hacemos" >Sobre nosotros</a></li>
                    <?php if (isset($_SESSION["user"]["mail"])): ?>
                        <li><a href="/app" ><i class="glyphicon glyphicon-search"></i> Buscar cultivos</a></li>
                    <?php endif; ?>
                </ul>


                <ul class="nav navbar-nav pull-right">
                    <?php if (isset($_SESSION["user"]["mail"])): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i> <?php echo (isset($_SESSION["user"]["name"]) ? $_SESSION["user"]["name"] : $_SESSION["user"]["mail"]) ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/user/profile">Mi Cuenta <small>editar</small></a></li>
                                <li><a href="/login/logout">Salir</a></li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li><a href="/login" >Iniciar Sesion</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </nav>
        <div class="container" >
            <?= $content ?>
        </div>

        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <ul class="list-inline">
                            <li><a href="/">Inicio</a></li>
                            <li>&nbsp;|&nbsp;</li>
                            <li><a href="/que-es">Sobre <span>qsgen</span></a></li>
                            <li>&nbsp;|&nbsp;</li>
                            <li><a href="/quienes-lo-hacemos">Sobre nosotros</a></li>
                            <li>&nbsp;|&nbsp;</li>
                            <li><a href="/login">Iniciar sesión</a></li>
                            <li>&nbsp;|&nbsp;</li>
                            <li><a href="/como-funciona">Ayuda</a>
                            <li>&nbsp;|&nbsp;</li>
                            <li><a href="/contacto">Contáctenos</a>
                        </ul>
                    </div>
                    <div class="col-sm-4 text-right">
                        <div>
                            Copyright <i class="glyphicon glyphicon-copyright-mark">2014 <span class="qsgen-logo" style="font-size: 2em;">qsgen</span></i>
                        </div>
                        <div style="margin-top: 5px;">
                            <a class="pull-right" href="http://www.gruposcientia.com.ar/" target="_blank"><img alt="Logo Grupo Scentia" title="Grupo Scenntia" src="/images/logo/gruposcientia.png" class="img-responsive img-rounded" width="170"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>