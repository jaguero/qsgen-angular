<div class="page row">
    <div class="page-header">
        <h1>Sobre nosotros</h1>
    </div>
    <div class="col-md-8">
        <h2>¿Quiénes hacemos <span class="qsgen-logo"style="font-size: 2em;">qsgen</span>?</h2>
        <p>
            El sistema de comparación de genotipos QSGen nació como una iniciativa de <a href="http://www.gruposcientia.com.ar" target="_blank">Grupo Scientia</a>. 
            Nuestra vocación por llevar a los productores herramientas sencillas pero robustas que faciliten la toma de decisiones nos 
            impulsó a pensar en la manera de sintetizar la información de los ensayos comparativos de rendimiento disponibles en todas las 
            regiones agroecológicas del país.
        </p>

        <h2>¿Cómo citarlo?</h2>

        <p>
            Menéndez F.J., Hasicic F., Agüero, J.M, Capodanno, I. (2014). QSGen, sistema de comparación de genotipos. <a href="http://www.qsgen.com.ar">www.qsgen.com.ar</a>.
        </p>
    </div>

</div>
