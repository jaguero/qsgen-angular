<link rel="stylesheet" type="text/css" href="/css/jquery.toastmessage.css">
<script type="text/javascript" src="/js/jquery.toastmessage.js"></script>
<div class="page row">
    <div class="page-header">
        <div class="container">
            <h1 lang="es">Contacto</h1>
        </div>
    </div>
    <div class="container">
        <div class="col-sm-8" >
            <div id="contact-form-container">
                <?php echo (isset($viewData["message"]) ? $viewData["message"] : ""); ?>
                <form role="form" class="form-horizontal" action="/page/contactar" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-4">Nombre</label>
                        <div class="col-sm-8">
                            <?php echo $viewData["form"]->getField("name") ?>
                            <span class="help-block"><?php echo $viewData["form"]->getFieldError("name") ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Mail</label>
                        <div class="col-sm-8">
                            <?php echo $viewData["form"]->getField("mail") ?>
                            <span class="help-block"><?php echo $viewData["form"]->getFieldError("mail") ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Mensaje</label>
                        <div class="col-sm-8">
                            <?php echo $viewData["form"]->getField("coment") ?>
                            <span class="help-block"><?php echo $viewData["form"]->getFieldError("coment") ?></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button class="btn btn-success btn-lg" >Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>