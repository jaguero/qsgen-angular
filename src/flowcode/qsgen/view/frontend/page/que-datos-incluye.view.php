<div class="page row">

    <div class="page-header">
        <h1>¿Qué datos incluye QSGen?</h1>
    </div>
    <p>
        El sistema QSGen incluye bases de datos de ensayos publicos (realizados por instituciones publicas o privadas)
        o por empresas agropecuarias. Los datos generados por empresas que venden semillas no son incluidos en ningun caso, de manera de mantener 
        la mayor objetividad posible.
    </p>

    <p>
        La calidad de los datos es cuidadosamente chequeada antes de su inclusión en nuestra base de datos.
    </p>

</div>