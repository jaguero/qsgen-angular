<div class="page row">
    <div class="page-header">
        <h1>Sobre QSgen</h1>
    </div>
    <div class="col-md-8">
        <h2>¿Qué es?</h2>
        <p>
            Es la mejor herramienta con la que cuentan actualmente en la Argentina los productores y técnicos para tomar las decisiones 
            sobre una de las tecnologías que en mayor medida afecta la rentabilidad de los planteos.
        </p>

        <h2>¿Por qué me conviene?</h2>
        <h3>Elegir un genotipo</h3>
        <p>
            La elección del genotipo es una tecnología clave de los sistemas de producción de cultivos extensivos como Soja, Maíz, Girasol, Trigo, etc. 
            Las diferencias en los rendimientos obtenidos debidas a características genéticas de las plantas pueden ser muy importantes y determinan una 
            gran parte de la competitividad de los cultivos. 
        </p>
        <h3>El genotipo y el medio ambiente</h3>
        <p>
            Sin embargo, elegir el mejor genotipo no es una tarea sencilla. Existe una fuerte interacción entre el genotipo y el ambiente en el cual crecen los cultivos, que contribuye muchas veces a dificultar el entendimiento de 
            las diferencias entre las variedades o híbridos. Esto significa que las diferencias entre dos variedades pueden cambiar notablemente 
            (incrementarse o decrecer) de acuerdo al nivel de productividad del sitio en el que han sido sembrados. Incluso, podría ocurrir el caso 
            en que un genotipo rindiera más o menos que otro dependiendo de las características del ambiente. 
        </p>
        <h3>Cantidad de datos</h3>
        <p>
            Por otro lado, la cantidad de datos que generalmente se encuentran disponibles para evaluar estas cuestiones es muy reducida y 
            no asegura la estabilidad de los resultados. Son necesarias muchas observaciones  (sitios y años) para poder generar relaciones de 
            rendimiento de los diferentes genotipos.
        </p>

        <p>
            Un problema grave que se genera para poder hacer las comparaciones es que las bases de datos generalmente se encuentran desbalanceadas. 
            Esto significa que no todos los genotipos fueron ensayados en todos los experimentos. Para poder hacer una comparación estadísticamente 
            válida es necesario seleccionar aquellos ensayos en los que estuvieran presentes todos los genotipos a evaluar.
        </p>

        <p>
            Por todo esto, está claro que la evaluación de las diferencias genéticas y de la interacción genotipo x ambiente es un tema complejo. 
        </p>

        <p>
            Todo esto es posible porque el sistema permite comparar genotipos de diferentes cultivos a partir de grandes bases de datos 
            (que incorporan tanto redes oficiales públicas como las realizadas por las más prestigiosas instituciones del país), y 
            eligiendo sólo los ensayos en los que se encuentran las genotipos seleccionados. Además, permite seleccionar los ensayos por zona 
            agroecológica y por campaña, y también de acuerdo a la fecha de siembra del ensayo, permitiendo un mejor entendimiento de la interacción 
            entre el ambiente y la genética. Para el cultivo de trigo, se incorpora además la posibilidad de discriminar los ensayos en los que se aplicaron 
            o no fungicidas para el control de enfermedades de hoja.
        </p>

    </div>
    <div class="col-md-4">
        <div class="well" style="opacity: 0.8;">
            <p>
                <strong>QSgen</strong> está pensado para que las personas encargadas de tomar las decisiones en los sistemas de producción puedan sortear todas 
                estas dificultades y sean capaces de responder preguntas tales como:
            </p>
            <ul class="list-unstyled">
                <li>
                    <i class="glyphicon glyphicon-ok"></i> ¿Qué diferencia de rendimiento puedo esperar entre los genotipos A y B en ambientes con niveles de productividad como los de mi campo?.
                </li>
                <li>
                    <i class="glyphicon glyphicon-ok"></i> ¿Tengo que cambiar el genotipo si cambio la fecha de siembra?.
                </li>
                <li>
                    <i class="glyphicon glyphicon-ok"></i> ¿Se justifica pagar la diferencia de costo en la bolsa de semillas de un determinado genotipo?.
                </li>
            </ul>
        </div>
        <div>
            <img src="/images/logo/qsgen-logo-cuadrado-transparente.png" class="img-responsive" title="qsgen">
        </div>
    </div>
</div>