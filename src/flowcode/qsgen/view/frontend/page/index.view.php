<div class="row" id="first-row">
    <div class="col-md-7" id="portada">
        <h3>La respuesta para la elección adecuada de genotipos</h3>
        <p>Utilizando toda la información disponible en su zona y en el país.</p>
    </div>
    <div class="col-md-4">
        <?php if (isset($_SESSION["user"])): ?>
            <a class="btn btn-info btn-block btn-lg btn-main" href="/app">
                <i class="glyphicon glyphicon-search"></i><br/>
                Buscar cultivos
            </a>
        <?php else: ?>
            <div class="well well-lg" id="login-box">
                <form class="form-horizontal" action="/login/register" method="post">
                    <div class="form-group">
                        <?php echo $viewData["register-form"]->getField("mail") ?>
                        <span class="help-block"><?php echo $viewData["register-form"]->getFieldError("mail") ?></span>
                    </div>

                    <div class="form-group">
                        <?php echo $viewData["register-form"]->getField("password") ?>
                        <span class="help-block"><?php echo $viewData["register-form"]->getFieldError("password") ?></span>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success btn-lg btn-block">¡Registrarme!</button>
                    </div>
                </form>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row" id="second-row">
    <div class="col-xs-6 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>¿Qué es QSGen?</h4>
            </div>
            <div class="panel-body">
                <p>La elección del genotipo es una tecnología clave de los sistemas de producción de cultivos extensivos...</p>
                <a class="btn btn-primary btn-block" href="/que-es">Ver más</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>¿Cómo funciona?</h4>
            </div>
            <div class="panel-body">
                <p>El sistema de comparación de genotipos QSGen tiene una interfaz amigable y simple de usar...</p>
                <a class="btn btn-primary btn-block" href="/como-funciona">Ver más</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>¿Qué datos incluye?</h4>
            </div>
            <div class="panel-body">
                <p>Incluye bases de datos de ensayos publicos o por empresas agropecuarias...</p>
                <a class="btn btn-primary btn-block" href="/que-datos-incluye">Ver más</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>¿Quiénes lo hacemos?</h4>
            </div>
            <div class="panel-body">

                <p>Nuestra vocación por llevar a los productores herramientas sencillas pero robustas...</p>
                <a class="btn btn-primary btn-block" href="/quienes-lo-hacemos">Ver más</a>
            </div>
        </div>
    </div>
</div>
<script>
    $("#mail").focus(function(e) {
        var message = $("#mail").next(".help-block");
        message.removeClass("error");
        message.removeClass("success");
        message.html('');
    });
    $("#mail").blur(function(e) {
        if ($("#mail").val().length > 0) {
            $.ajax({
                url: "/user/validMail",
                type: "post",
                data: {"mail": $("#mail").val()},
                success: function(data) {
                    var message = $("#mail").next(".help-block");
                    switch (data) {
                        case 0:
                            message.addClass("error");
                            message.html('<i class="glyphicon glyphicon-exclamation-sign"></i> <small>Mail no valido.</small>');
                            break;
                        case 1:
                            message.addClass("error");
                            message.html('<i class="glyphicon glyphicon-ban-circle"></i> <small>Mail no disponible.</small>');
                            break;
                        case 2:
                            message.addClass("success");
                            message.html('<i class="glyphicon glyphicon-ok"></i>');
                            break;
                    }
                },
                error: function() {
                    console.log("user service is unreachable.");
                },
            });
        }
    });
</script>
