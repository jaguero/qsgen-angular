<div class="page row">

    <div class="page-header">
        <h1>Ayuda</h1>
    </div>
    <div class="col-md-8">

        <h2>¿Cómo funciona?</h2>

        <p>
            La primera pantalla le permite <strong>registrarse</strong> en el sistema. Después de ingresar su <strong>dirección de e-mail</strong> y <strong>contraseña</strong> recibirá un e-mail en su casilla con un link para confirmar su registro. A partir de este momento usted podrá iniciar sesión usando el botón ubicado en la parte superior derecha de la pantalla.
        </p>


        <h2>¿Cómo buscar?</h2>

        <p>
            Luego de iniciar sesión el sistema lo conduce a una pantalla intermedia en la que deberá oprimir el botón <strong>Buscar Cultivos</strong>.
        </p>

        <div>
            <h1>Selección de datos</h1>
            <p>En esta pantalla podrá seleccionar:</p>

            <ul>
                <li>
                    <strong>Zona:</strong> Las diferentes regiones agroecológicas de la Argentina fueron divididas zonas. Usted podrá elegir los ensayos de genotipos de una, varias o todas las zonas para realizar las comparaciones.
                </li>
                <li>
                    <strong>Campaña:</strong> del mismo modo que para la zona, se pueden seleccionar una, varias o todas las campañas para las que existen datos. Las campañas corresponden a los ensayos sembrados entre Febrero de ese año y Enero del siguiente.
                </li>  
                <li>
                    <strong>Periodo de siembra:</strong> en esta sección usted puede acotar las fechas de siembra de los ensayos que le interesa comparar.
                </li>
                <li>
                    <strong>Condición del ensayo:</strong> Actualmente sólo para el caso del cultivo de Trigo se puede seleccionar la opción para que el sistema elija los ensayos en los que se aplicaron fungicidas para el control de enfermedades de hoja o aquellos en los que este control no se realizó. También es posible elegir todos independientemente de las medidas de protección utilizadas.
                </li>
                <li>
                    <strong>Genotipos:</strong> Se deben seleccionar los genotipos que desea comparar. El sistema comparar de 1 a 5 genotipos simultáneamente.
                </li>
            </ul>
        </div>

        <p>
            A lo largo de las diferentes elecciones de condiciones del ensayo que usted va eligiendo, el sistema va restringiendo la base de datos a aquellos ensayos que cumplen con estas condiciones. De manera tal que si usted elije la zona “Centro” y no existen ensayos en los que se hubiera sembrado el genotipo “x” en esa zona, la opción para elegirlo no aparecerá en la lista de genotipos. De esta manera se evita realizar búsquedas que den resultados nulos.
        </p>




        <h2>¿Cómo trabaja el sistema?</h2>

        <p>
            A partir de la selección de zona, campaña, periodo de siembra y genotipo que usted hubiera realizado, el sistema busca en la base de datos todos los ensayos que cumplan con esas condiciones. Para el caso de los genotipos, la condición para elegir un ensayo es que todos los genotipos seleccionados estén sembrados en él. De esta manera se asegura que el análisis estadístico es válido ya que no existen desbalances en la base utilizada.
        </p>

        <p>
            Los resultados se presentan de diferentes maneras. La primera corresponde con un análisis de interacción del genotipo con el ambiente como el propuesto por de Wit (1965). Este análisis muestra cómo es la relación entre el rendimiento de un determinado genotipo y la productividad del ambiente. Como la productividad del ambiente es un concepto relativo a la forma en que se lo mida, en este tipo de análisis se utiliza como indicador al “índice ambiental”. El índice ambiental es el promedio de los rendimientos de todos los genotipos seleccionados en cada ensayo. El grafico que se presenta muestra cual es la relación que existe entre el rendimiento de un determinado genotipo, y la productividad estimada del ambiente (índice ambiental).
        </p>

        <p>
            En la tabla que se presenta debajo del grafico, se muestran los parámetros de las regresiones entre estas dos variables para cada uno de los genotipos seleccionados (Intersección y pendiente) Pendientes superiores a 1 indican que el incremento en el rendimiento del genotipo conforme mejora la calidad del ambiente es superior al incremento medio de todos los genotipos seleccionados. Del mismo modo, la caída en los rendimientos que experimenta este genotipo a medida que decrece la productividad del ambiente es también superior a la caída en los rendimientos de la media de los genotipos seleccionados. Por otro lado, pendientes inferiores a 1, indican el comportamiento contrario al mencionado.    </p>
        <p>
            Ni la pendiente ni la ordenada al origen indican per se la superioridad de un genotipo sobre otro en un ambiente determinado. Para conocer cuál de dos genotipos presenta mayor rendimiento en un ambiente de un nivel de productividad determinado, se debe observar cuál es el valor que adquiere la función (valor del rendimiento, en el eje vertical) para ambos genotipos en el nivel de productividad del ambiente para el cual se desea obtener información (valor del índice ambiental en el eje horizontal del grafico).
        </p>

        <p>
            La tabla adjunta presenta también el rendimiento promedio de cada genotipo en todos los ensayos seleccionados (que cumplen con las condiciones que usted indicó durante la selección). Para el caso del cultivo de girasol el rendimiento corresponde al rendimiento ajustado por contenido de aceite. Para algunos cultivos, se muestran además otras características que se consideran importantes en el momento de seleccionar materiales genéticos. Así, el cultivo de cebada cuenta con información de % de proteína, y para el cultivo de Girasol se informa el % de aceite. 
        </p>

        <p>
            Al costado del grafico se muestra además el dato de cuántos ensayos fueron seleccionados.  
        </p>

        <p>
            Arriba del grafico encontrará la solapa “ensayos”.  Esta solapa lo llevará a una pantalla en la que encontrará los datos de rendimiento de todos los genotipos en cada uno de los ensayos que cumplen con las condiciones que usted seleccionó.
        </p>

        <h2>Nueva Búsqueda</h2>
        <p>
            Para realizar una nueva comparación de genotipos, debe oprimir el botón <strong>Nueva Búsqueda</strong>. 
            Este botón lo llevará a la pantalla de selección de condiciones donde usted podrá cambiar uno o varias de las condiciones de los ensayos seleccionados.
        </p>

    </div>

</div>