<div class="page row">
    <div class="page-header">
        <h1>Mi Cuenta <small><?php echo (isset($_SESSION["user"]["username"]) ? $_SESSION["user"]["username"] : "") ?></small></h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="/user/profile">Cuenta</a></li>
                <li><a href="/user/password">Password</a></li>
            </ul>
        </div>
        <div class="col-md-8">

            <form class="form-horizontal" role="form" action="/user/updateProfile" method="post">
                <?php echo (isset($viewData["message"]) ? $viewData["message"] : ""); ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nombre</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("nombre") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("nombre") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Mail</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("mail") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("mail") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Pais</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("pais") ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Compañia</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("compania") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("compania") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Cargo</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("cargo") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("cargo") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button class="btn btn-success">Guardar cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>