<div class="row" style="min-height: 500px;">
    <div class="col-md-4 col-md-offset-4">
        <div class="well well-lg">
            <h1>Ingresar</h1>
            <?php echo (isset($viewData["message"]) ? $viewData["message"] : ""); ?>
            <form class="form-horizontal" action="/login/validlogin" method="post">
                <div class="form-group">
                    <?php echo $viewData["form"]->getField("mail") ?>
                    <span class="help-block"><?php echo $viewData["form"]->getFieldError("mail") ?></span>
                </div>
                <div class="form-group">
                    <?php echo $viewData["form"]->getField("password") ?>
                    <span class="help-block"><?php echo $viewData["form"]->getFieldError("password") ?></span>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-block" type="submit">Ingresar</button>
                    <a class="btn btn-link" href="/user/passForgot">Olvide mi contraseña</a>
                </div>
            </form>
        </div>
    </div>
</div>