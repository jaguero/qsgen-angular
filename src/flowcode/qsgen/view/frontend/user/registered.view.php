<div class="page">
    <div class="page-header">
        <h1>Usuario</h1>
    </div>
    <div class="alert alert-success">
        <p>¡Se ha registrado con éxito!</p>
    </div>

    <div class="alert alert-info">
        <strong>Importante: </strong>
        <p>Se le ha enviado un mail con un link, al cual debe acceder para que verifiquemos su mail.</p>
        <p>Una vez que haga esto podrá ingresar al sistema.</p>
    </div>

</div>