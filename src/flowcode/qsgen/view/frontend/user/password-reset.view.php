<div class="page row">
    <div class="page-header">
        <h1>Mi Cuenta <small><?php echo (isset($_SESSION["user"]["username"]) ? $_SESSION["user"]["username"] : "") ?></small></h1>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php echo (isset($viewData["message"]) ? $viewData["message"] : ""); ?>
            <form class="form-horizontal" role="form" action="/user/passReset" method="post">
                <div class="form-group">
                    <label class="col-md-3 control-label">Mail</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("mail") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("mail") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button class="btn btn-default"><i class="glyphicon glyphicon-warning-sign"></i> Recuperar Contraseña</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>