<div class="page row">
    <div class="page-header">
        <h1>Mi Cuenta <small><?php echo (isset($_SESSION["user"]["username"]) ? $_SESSION["user"]["username"] : "") ?></small></h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/user/profile">Cuenta</a></li>
                <li class="active"><a href="/user/password">Password</a></li>
            </ul>
        </div>
        <div class="col-md-8">
            <?php echo (isset($viewData["message"]) ? $viewData["message"] : ""); ?>
            <form class="form-horizontal" role="form" action="/user/passChange" method="post">
                <div class="form-group">
                    <label class="col-md-3 control-label">Contraseña actual</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("password-old") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("password-old") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Contraseña nueva</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("password-new") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("password-new") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Repetir contraseña</label>
                    <div class="col-md-5">
                        <?php echo $viewData["form"]->getField("password-confirm") ?>
                        <span class="help-block"><?php echo $viewData["form"]->getFieldError("password-confirm") ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button class="btn btn-success">Guardar cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>