<?php

use flowcode\ceibo\data\PDOMySqlDataSource;
use flowcode\ceibo\EntityManager;
use flowcode\enlace\Enlace;

/*
$allowed = array();
//$allowed[] = "186.153.169.204";
//$allowed[] = "190.193.224.173";
//$allowed[] = "83.52.149.160";
//$allowed[] = "83.138.253.168";

$ip = $_SERVER['REMOTE_ADDR'];

if (!in_array($ip, $allowed)) {
    header("Location: http://zaragozadenoche.es/index.php");
    exit;
}*/

require './vendor/autoload.php';

$app = new Enlace(Enlace::$MODE_DEVELOPMENT);

/** setup */
$app->set("dir", array(
    "src" => __DIR__ . "/src",
    "log" => __DIR__ . "/log"
));
$app->set("scanneableControllers", array(
    "qsgen" => "\\flowcode\\qsgen\\controller\\"
));
$app->set("defaultController", "\\flowcode\\qsgen\\controller\\PageController");
$app->set("defaultMethod", "manage");
$app->set("errorMethod", "errorMethod");

$app->set("loginController", "\\flowcode\\qsgen\\controller\\AdminLoginController");
$app->set("loginMethod", "index");
$app->set("restrictedMethod", "restricted");

/** views layouts */
$app->set("view", array(
    "path" => __DIR__ . "/src/flowcode/qsgen/view",
    "layout" => array(
        "frontend" => "frontend",
        "backend" => "backend",
    )
));

/** data access */
$dataSource = new PDOMySqlDataSource();
$dataSource->setDbDsn("mysql:host=localhost;dbname=qsgen");
$dataSource->setDbUser("root");
$dataSource->setDbPass("root");

$em = EntityManager::getInstance();
$em->setDataSource($dataSource);
$em->setMappingFilePath(__DIR__ . "/config/db/ceibo-mapping.xml");

/** other config */
$app->set("mail", array(
    "notifications" => "jaguero@flowcode.com.ar",
));
$app->set("mailcontacto", "jaguero@flowcode.com.ar");
$app->set("sitename", "QSGen");

/** routes */
require_once './config/routes.php';

/** run app */
$app->handleRequest($_SERVER['REQUEST_URI']);
?>
