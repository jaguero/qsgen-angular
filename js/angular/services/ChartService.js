angular.module('qsgen').factory('ChartService', function() {
    return {
        addSeparatorsNF: function(nStr, inD, outD, sep) {
            nStr += '';
            var dpos = nStr.indexOf(inD);
            var nStrEnd = '';
            if (dpos != -1) {
                nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
                nStr = nStr.substring(0, dpos);
            }
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(nStr)) {
                nStr = nStr.replace(rgx, '$1' + sep + '$2');
            }
            if (nStrEnd == outD + '00')
                return nStr;
            else
                return nStr + nStrEnd;
        },
        getChart: function(genotipos, ensayos) {

            var chart_data_xml = '<chart_data><row><null/>';
            for (e in genotipos[0]['ensayos']) {
                chart_data_xml += '<string>x</string>';
                chart_data_xml += '<string>y</string>';
            }
            chart_data_xml += '</row>';

            for (i in genotipos) {
                chart_data_xml += '<row>';
                chart_data_xml += '<string>' + genotipos[i]['nombre'] + '</string>';
                for (e in genotipos[i]['ensayos']) {
                    chart_data_xml += '<number shadow="low" tooltip="P=' + this.addSeparatorsNF(parseFloat(ensayos[e]['rendimiento_promedio']).toFixed(2), '.', ',', '.') + ' kg/ha\nR=' + this.addSeparatorsNF(parseFloat(genotipos[i]['ensayos'][e]['rendimiento']).toFixed(2), '.', ',', '.') + ' kg/ha">' + parseFloat(ensayos[e]['rendimiento_promedio']).toFixed(2) + '</number>';
                    chart_data_xml += '<number>' + parseFloat(genotipos[i]['ensayos'][e]['rendimiento']).toFixed(2) + '</number>';
                }
                chart_data_xml += '</row>';
            }
            chart_data_xml += '</chart_data>';

            var chart_xml = '';
            chart_xml += '<chart>';
            chart_xml += '<axis_category shadow="low" size="12" color="000000" alpha="85" orientation="horizontal" decimals="0" separator="." decimal_char="," />';
            chart_xml += '<axis_ticks value_ticks="false" category_ticks="true" major_thickness="1" minor_thickness="0" minor_count="1" major_color="000000" minor_color="222222" position="inside" />';
            chart_xml += '<axis_value min="0" max="12" size="10" color="ffffff" alpha="60" prefix="" suffix="" decimals="0" separator="." decimal_char="," show_min="false" />';
            chart_xml += '<chart_border color="222222" top_thickness="0" bottom_thickness="3" left_thickness="0" right_thickness="0" />';
            chart_xml += chart_data_xml;
            chart_xml += '<chart_grid_h alpha="10" color="000000" thickness="1" />';
            chart_xml += '<chart_grid_v alpha="20" color="000000" thickness="2" type="dotted" />';
            chart_xml += '<chart_guide radius="6" line_color="ff4400" line_alpha="90" line_thickness="2" />';
            chart_xml += '<chart_pref point_size="7" trend_alpha="30" trend_thickness="3" />';
            chart_xml += '<chart_rect x="70" y="55" width="380" height="180" positive_color="000000" positive_alpha="25" />';
            chart_xml += '<chart_type>scatter</chart_type>';
            chart_xml += '<draw>';
            chart_xml += '<rect shadow="shadow2" layer="background" x="70" y="55" width="380" height="180" fill_color="000000" fill_alpha="100" line_alpha="0" line_thickness="0" />';
            chart_xml += '<text shadow="high" color="ffffff" alpha="30" size="18" x="70" y="265" width="400" height="150" h_align="left" v_align="top">Promedio (kg/ha)</text>';
            chart_xml += '<text shadow="high" color="ffffff" alpha="30" rotation="-90" size="18" x="-30" y="237" width="200" height="60" v_align="bottom">Rendimiento (kg/ha)</text>';
            chart_xml += '</draw>';
            chart_xml += '<filter>';
            chart_xml += '<shadow id="shadow1" inner="true" quality="1" distance="50" angle="135" color="000000" alpha="20" blurX="300" blurY="200" />';
            chart_xml += '<shadow id="shadow2" distance="7" angle="70" alpha="35" blurX="12" blurY="8" knockout="true" />';
            chart_xml += '<shadow id="low" distance="2" angle="45" alpha="50" blurX="5" blurY="5" />';
            chart_xml += '<shadow id="high" distance="5" angle="45" alpha="75" blurX="10" blurY="10" />';
            chart_xml += '</filter>';
            chart_xml += '<legend  shadow="low" x="10" y="10" width="460" height="35" margin="3" fill_color="ffffff" fill_alpha="0" line_color="000000" line_alpha="0" line_thickness="0" size="12" color="ffffff" alpha="75" />';
            chart_xml += '<tooltip color="ff4400" alpha="80" size="12" background_alpha="0" shadow="low" />';
            chart_xml += '<series_color>';
            chart_xml += '<color>88FF00</color>';
            chart_xml += '<color>FF4400</color>';
            chart_xml += '<color>FF8800</color>';
            chart_xml += '<color>800080</color>';
            chart_xml += '<color>00FFFF</color>';
            chart_xml += '</series_color>';
            chart_xml += '</chart>';

            return chart_xml;
        },
    };
});