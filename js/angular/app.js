// Declare app level module which depends on filters, and services
var qsgen = angular.module('qsgen', ['ngRoute']);

var hide_layout = function(location) {
    return location.path().indexOf("sign") > 0;
}
qsgen.config(function($routeProvider) {
    var basePath = "js/angular/";

    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    /* app */
    $routeProvider.when('/', {templateUrl: basePath + 'views/app/inicio.html', controller: 'AppController'});
    $routeProvider.when('/result', {templateUrl: basePath + 'views/app/inicio.html', controller: 'AppController'});


    $routeProvider.otherwise({redirectTo: '/'});

}).run();

