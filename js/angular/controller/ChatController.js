angular.module('qsgen').controller("ChatController", function($scope, ChatService) {
    $scope.current_user = {name: "Alfredo", id_member: 1, status: "connected", image: "/images/default-avatar.gif"};

    /* FIXME */
    var ambito = {"ambito_id": "1", chat: {id: 1, messages: []}};
    var lastCallTimestamp = "1000";

    /** init values */
    $scope.users = [];
    $scope.chat_list_open = false;
    $scope.selected_user = false;
    $scope.message = {};

    var messages = [];

    $scope.updateChat = function() {
        ChatService.updateChat(ambito, lastCallTimestamp,
                function(data) {
                    messages = data.messages;
                    console.log(messages);
                },
                function() {
                    console.log("updateChat: chat service is dead.");
                });
    };

    $scope.getMembers = function() {
        ChatService.getMembers(
                ambito,
                function(data) {
                    $scope.users = data.members;
                },
                function() {
                    console.log("getMembers: chat service is dead.");
                });
    };

    $scope.find_user_by_id = function(id_member) {
        for (user in $scope.users) {
            if ($scope.users[user].id_member === id_member) {
                return $scope.users[user];
            }
        }
    };

    $scope.append_message = function() {
        //push message
        ChatService.sendMessage(ambito, $scope.current_user.id, $scope.message.text,
                function() {
                    $scope.message.text = "";
                    $scope.updateChat();
                });
    }

    $scope.messages_with = function() {
        return messages[$scope.selected_user.id_member];
    }

    $scope.open_chat = function(id_member) {
        $scope.selected_user = $scope.find_user_by_id(id_member);
    };

    $scope.close_chat = function() {
        $scope.selected_user = false;
    };

    $scope.toggle_chat_list = function() {
        if (!$scope.chat_list_open) {
            $scope.getMembers();
        }
        $scope.chat_list_open = !$scope.chat_list_open;
    }
});
