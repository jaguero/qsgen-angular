angular.module('qsgen').controller("AppController", function($scope, $http, $rootScope) {
    clearZonas();
    clearCampanas();
    clearGenotipos();

    /* inicializamos fechas */
    $scope.dayFrom = "01";
    $scope.monthFrom = "01";
    $scope.dayTo = "31";
    $scope.monthTo = "12";
    $scope.dateFrom = $scope.dayFrom + "-" + $scope.monthFrom;
    $scope.dateTo = $scope.dayTo + "-" + $scope.monthTo;           

    $scope.genotipoSelected = [];

    $scope.cultivo = null;
    $scope.fungicida = -1;
    $scope.viewingResults = false;
    $scope.canFilterDate = false;
    $scope.errorPeriodo = false;
    $scope.chart = null;
    $scope.botonPeriodos = {
        "textoError": "Periodo Invalido",
        "textoDefault": 'Siguiente',
        "textoValue": "Siguiente"
    };

    $('#cultivo-selection').focus();

    /**
     * Load cultivos
     */
    $http.get("/cultivo").success(function(data) {
        $scope.cultivos = data;
    });

    $scope.readyZonas = false;
    $scope.readyCampanias = false;
    $scope.readyPeriodos = false;
    $scope.readyGenotipos = false;

    $scope.spinnerZonas = false;
    $scope.spinnerPeriodos = false;
    $scope.spinnerCampanias = false;
    $scope.spinnerGenotipos = false;

    /**
     * Select Cultivo
     * @returns {undefined}
     */
    $scope.select_cultivo = function() {
        clearZonas();
        clearCampanas();
        clearGenotipos();
        $scope.spinnerZonas = true;
        $.ajax({
            url: "/zona/cultivo/" + $scope.cultivo.id,
            type: "GET",
            success: function(data) {
                $scope.zonas = data;
                for (var i in data) {
                    $scope.zonas_selected.push(data[i].id);
                }
                $scope.spinnerZonas = false;
                $scope.readyZonas = true;
                $scope.$apply();
                $('.mapa-zonas').imgPreview({
                    containerID: 'imgPreviewWithStyles',
                    imgCSS: {
                        height: 418,
                        width: 396
                    },
                    onShow: function(link) {
                        $('<span>' + $(link).text() + '</span>').prependTo(this);
                    },
                    onHide: function(link) {
                        $('span', this).remove();
                    }
                });
            }
        });
    };

    /* toggle zona */
    $scope.toggleZonaSel = function(id_zona) {
        var idx = $scope.zonas_selected.indexOf(id_zona);
        if (idx > -1) {
            $scope.zonas_selected.splice(idx, 1);
        } else {
            $scope.zonas_selected.push(id_zona);
        }
    };


    /**
     * Toggle campana
     * @param {type} id_campana
     * @returns {undefined}
     */
    $scope.toggleCampanaSel = function(id_campana) {
        var idx = $scope.campanas_selected.indexOf(id_campana);
        if (idx > -1) {
            $scope.campanas_selected.splice(idx, 1);
        } else {
            $scope.campanas_selected.push(id_campana);
        }
    };

    $scope.filter_zona = function() {
        clearCampanas();
        clearGenotipos();
        $scope.spinnerCampanias = true;
        $.ajax({
            url: "/ensayo/campanas",
            type: "POST",
            data: {
                "cultivo": $scope.cultivo.id,
                "zonas": $scope.zonas_selected,
            },
            success: function(data) {
                $scope.campanas = data;
                for (var i in data) {
                    $scope.campanas_selected.push(data[i].campana);
                }
                $scope.readyCampanias = true;
                $scope.$apply();
            },
            error: function(a, b, c) {
                console.log("error");
                console.log(a);
            },
            complete: function() {
                $scope.spinnerCampanias = false;
                $scope.$apply();
            }
        });

    }
    $scope.filter_campana = function() {
        $scope.canFilterDate = true;
        $scope.readyPeriodos = true;

    }

    $scope.validPeriodo = function() {
        $scope.errorPeriodo = false;
        $scope.readyPeriodos = true;
        $scope.botonPeriodos.textoValue = $scope.botonPeriodos.textoDefault;

        if (($scope.monthTo < $scope.monthFrom)
                || ($scope.monthTo == $scope.monthFrom && $scope.dayTo < $scope.dayFrom)
                || ($scope.monthFrom == 2 && $scope.dayFrom > 29 || $scope.monthTo == 2 && $scope.dayTo > 29)
                ) {
            $scope.errorPeriodo = true;
            $scope.readyPeriodos = false;
            $scope.botonPeriodos.textoValue = $scope.botonPeriodos.textoError;
        }

    }

    $scope.filter_fungicida = function() {
        $scope.gen1 = null;
        $scope.filter_genotipos1();
    }

    $scope.filter_genotipos1 = function() {
        $scope.gen1 = null;
        $scope.gen2 = null;
        $scope.gen3 = null;
        $scope.gen4 = null;
        $scope.gen5 = null;
        $scope.spinnerGenotipos = true;
        $.ajax({
            url: "/genotipo/find",
            type: "POST",
            data: getDataFilterGenotipos(),
            success: function(data) {
                if (data.length > 0) {
                    $scope.genotipos1 = data;
                    $scope.readyGenotipos = true;
                }
            },
            error: function(a, b, c) {
                console.log(a);
            },
            complete: function() {
                $scope.spinnerGenotipos = false;
                $scope.$apply();
            }
        });
    }

    $scope.filter_genotipos2 = function() {
        $scope.gen2 = null;
        $scope.gen3 = null;
        $scope.gen4 = null;
        $scope.gen5 = null;
        $scope.spinnerGenotipos = true;
        $.ajax({
            url: "/genotipo/find",
            type: "POST",
            data: getDataFilterGenotipos(),
            success: function(data) {
                if (data.length > 0) {
                    $scope.genotipos2 = data;
                }
            },
            error: function(a, b, c) {
                console.log(a);
            },
            complete: function() {
                $scope.spinnerGenotipos = false;
                $scope.$apply();
            }
        });
    }


    $scope.filter_genotipos3 = function() {
        $scope.gen3 = null;
        $scope.gen4 = null;
        $scope.gen5 = null;
        $scope.spinnerGenotipos = true;
        $.ajax({
            url: "/genotipo/find",
            type: "POST",
            data: getDataFilterGenotipos(),
            success: function(data) {
                if (data.length > 0) {
                    $scope.genotipos3 = data;
                    $scope.readyGenotipos = true;
                }
            },
            error: function(a, b, c) {
                console.log(a);
            },
            complete: function() {
                $scope.spinnerGenotipos = false;
                $scope.$apply();
            }
        });
    }

    $scope.filter_genotipos4 = function() {
        $scope.gen4 = null;
        $scope.gen5 = null;
        $scope.spinnerGenotipos = true;
        $.ajax({
            url: "/genotipo/find",
            type: "POST",
            data: getDataFilterGenotipos(),
            success: function(data) {
                if (data.length > 0) {
                    $scope.genotipos4 = data;
                    $scope.readyGenotipos = true;
                }
            },
            error: function(a, b, c) {
                console.log(a);
            },
            complete: function() {
                $scope.spinnerGenotipos = false;
                $scope.$apply();
            }
        });
    }

    $scope.filter_genotipos5 = function() {
        $scope.gen5 = null;
        $scope.spinnerGenotipos = true;
        $.ajax({
            url: "/genotipo/find",
            type: "POST",
            data: getDataFilterGenotipos(),
            success: function(data) {
                if (data.length > 0) {
                    $scope.genotipos5 = data;
                    $scope.readyGenotipos = true;
                }
            },
            error: function(a, b, c) {
                console.log(a);
            },
            complete: function() {
                $scope.spinnerGenotipos = false;
                $scope.$apply();
            }
        });
    }


    $scope.toggleSearchBox = function() {

        $scope.viewingResults = !$scope.viewingResults;
    }

    /* main search */
    $scope.search = function() {
        updateGenotipos();

        $.ajax({
            url: "/ensayo/resultado",
            type: "POST",
            data: {
                "cultivo": $scope.cultivo.id,
                "campanas": $scope.campanas_selected,
                "zonas": $scope.zonas_selected,
                "dateFrom": "1986-" + $scope.monthFrom + "-" + $scope.dayFrom,
                "dateTo": "1986-" + $scope.monthTo + "-" + $scope.dayTo,
                "genotipos": $scope.genotipoSelected,
                "fungicida": $scope.fungicida,
            },
            success: function(data) {
                var colors = ["#f5a899", "#00ff00", "#0000ff", "#68744", ""];
                $scope.resultInstance = data;
                $scope.viewingResults = true;
                $scope.populateEnsayos();
                $scope.$apply();
                var dataSource = [];
                for (var g in data.genotipos) {
                    var genotipoData = [];
                    var genotipoRectaData = [];
                    for (var i in data.genotipos[g].ensayos) {
                        genotipoData.push([data.ensayos[i].rendimiento_promedio, data.genotipos[g].ensayos[i].rendimiento]);
                        var xRecta = data.ensayos[i].rendimiento_promedio;
                        var yRecta = (data.genotipos[g].pendiente_recta * data.ensayos[i].rendimiento_promedio) + data.genotipos[g].interseccion;
                        genotipoRectaData.push([xRecta, yRecta]);
                    }
                    dataSource.push({
                        label: data.genotipos[g].nombre,
                        data: genotipoData,
                        lines: {show: false}, points: {show: true},
                        color: colors[g],
                    });
                    dataSource.push({
                        data: genotipoRectaData,
                        points: {show: false},
                        color: colors[g],
                    });
                }
                $.plot("#placeholder", dataSource, {
                    series: {
                        lines: {show: true},
                        points: {show: true}
                    },
                });
                var xaxisLabel = $("<div class='axisLabel xaxisLabel'></div>")
                        .text("Rendimiento medio del ambiente (kg/ha)")
                        .appendTo($('.chart-container'));

                var yaxisLabel = $("<div class='axisLabel yaxisLabel'></div>")
                        .text("Rendimiento (kg/ha)")
                        .appendTo($('.chart-container'));
                $('#myTab a').click(function(e) {
                    e.preventDefault()
                    $(this).tab('show')
                });
            },
            error: function(a, b, c) {
                console.log("error")
                console.log(a);
            }
        });

    }

    $scope.populateEnsayos = function() {
        $scope.ensayos = [];
        $scope.ensayosGenotipos = [];

        var ensayosIds = [];
        for (var ensid in $scope.resultInstance.ensayos) {
            ensayosIds.push(ensid);
        }

        $.ajax({
            url: "/ensayo/findAllById",
            type: "POST",
            data: {
                "ensayos": ensayosIds,
            },
            success: function(data) {
                $scope.ensayos = data;
                $scope.$apply();
                $.ajax({
                    url: "/ensayo/ensayosGenotipos",
                    type: "POST",
                    data: {
                        "ensayos": ensayosIds,
                    },
                    success: function(data) {
                        $scope.ensayosGenotipos = data;
                        $scope.$apply();
                    },
                    error: function(a, b, c) {
                        console.log("error")
                        console.log(a);
                    }
                });
            },
            error: function(a, b, c) {
                console.log("error")
                console.log(a);
            }
        });


    }
    /* auxiliary functions */
    function getDataFilterGenotipos() {
        updateGenotipos();
        var data = {
            "cultivo": $scope.cultivo.id,
            "campanas": $scope.campanas_selected,
            "zonas": $scope.zonas_selected,
            "dateFrom": "1986-" + $scope.monthFrom + "-" + $scope.dayFrom,
            "dateTo": "1986-" + $scope.monthTo + "-" + $scope.dayTo,
            "genotipos": $scope.genotipoSelected,
            "fungicida": $scope.fungicida,
        };
        return data;
    }
    function updateGenotipos() {
        $scope.genotipoSelected = [];
        if ($scope.gen1 !== null) {
            $scope.genotipoSelected.push($scope.gen1);
        }
        if ($scope.gen2 !== null) {
            $scope.genotipoSelected.push($scope.gen2);
        }
        if ($scope.gen3 !== null) {
            $scope.genotipoSelected.push($scope.gen3);
        }
        if ($scope.gen4 !== null) {
            $scope.genotipoSelected.push($scope.gen4);
        }
        if ($scope.gen5 !== null) {
            $scope.genotipoSelected.push($scope.gen5);
        }

    }

    function clearGenotipos() {
        $scope.genotipos1 = [];
        $scope.genotipos2 = [];
        $scope.genotipos3 = [];
        $scope.genotipos4 = [];
        $scope.genotipos5 = [];
        $scope.gen1 = null;
        $scope.gen2 = null;
        $scope.gen3 = null;
        $scope.gen4 = null;
        $scope.gen5 = null;
    }

    function clearZonas() {
        $scope.zonas = [];
        $scope.zonas_selected = [];
    }

    function clearCampanas() {
        $scope.campanas = [];
        $scope.campanas_selected = [];
    }




});
